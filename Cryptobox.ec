require import AllCore Real Distr List SmtMap FSet.
require (*--*) NIKE PKAE NBSES.

pragma +implicits.

(**** Parameters ****)
(*** Nonce-Based Authenticated Encryption ***)
(** Plaintexts, ciphertexts **)
type ptxt, ctxt.
op length : { ptxt -> int | forall p, 0 <= length p } as ge0_length.
op dctxt  : { int -> ctxt distr | forall n, 0 <= n => is_lossless (dctxt n) } as dctxt_ll.

(** Keys, additional data, nonces **)
type key, adata, nonce.
op dkey : { key distr | is_lossless dkey } as dkey_ll.

(*** Non-Interactive Key Exchange ***)
(** Public keys **)
type pkey, skey.

op dkp : { (pkey * skey) distr | is_lossless dkp } as dkp_ll.
axiom dkpI sk sk' pk :
     (pk, sk)  \in dkp
  => (pk, sk') \in dkp
  => sk = sk'.

(** TODO: bound the guessing entropy **)

(** The proof only works if the NIKE is truly NI: its semantics must
    be expressible as operators (formally, because we use it in
    formulas, but also informally because we need it to produce the
    same concrete output (not just distribution over output) whenever
    it's called on the same inputs, since the deconstruction relies on
    storing results after instead of recomputing them every time).
**)
op skgen : pkey -> skey -> key.
axiom skgen_correct sk1 sk2 pk1 pk2 :
     (pk1, sk1) \in dkp
  => (pk2, sk2) \in dkp
  => skgen pk1 sk2 = skgen pk2 sk1.

(* This is important for the proof, but note that we only care that
   such a function exists! *)
op sort : pkey -> pkey -> pkey * pkey.
axiom sort_is_permutation X X' Y Y' :
      sort X Y = sort X' Y'
  <=> (   (X = X' /\ Y = Y')
       \/ (X = Y' /\ Y = X')).

(*** Instantiating Interfaces ***)
clone import NIKE as NIKE' with
  type    pkey <- pkey,
  type    skey <- skey,
  type     key <- key,
  type     sid <- pkey * pkey,
    op get_sid <- sort,
    op     dkp <- dkp,
    op   skgen <- skgen
proof
  get_sidP by (move=> X Y X' Y'; exact: sort_is_permutation),
  dkp_ll   by exact: dkp_ll,
  dkpI     by exact: dkpI,
  skgen_correct by exact: skgen_correct.

clone include NBSES with
  type key   <- key,
  type adata <- adata,
  type nonce <- nonce,
  type ptxt  <- ptxt,
  type ctxt  <- ctxt
proof *.

clone import PKAE as Pkae with
   type pkey <- pkey,
   type skey <- skey,
  type adata <- adata,
  type nonce <- nonce,
   type ptxt <- ptxt,
   type ctxt <- ctxt,
   op length <- length,
    op dctxt <- dctxt,
     op sort <- sort
proof
  ge0_length          by exact/ge0_length,
  sort_is_permutation by exact/sort_is_permutation,
  *.

(*** Defining Cryptobox ***)
module Cryptobox (E : NBSES) = {
  proc pkgen() = {
    var kp;

    kp <$ dkp;
    return kp;
  }

  proc enc(sk, pk, d, p, n) = {
    var ssk, c;

    ssk <- skgen pk sk;
      c <@ E.enc(d, p, n, ssk);
    return Some c;
  }

  proc dec(sk, pk, d, c, n) = {
    var ssk, p;

    ssk <- skgen pk sk;
      p <@ E.dec(d, c, n, ssk);
    return p;
  }
}.

(** Our core deconstruction and its interfaces **)
module type GMODPKAE_in = {
  proc gen() : pkey option
  proc csetpk(_ : pkey) : unit option

  proc genssk(_ : pkey, _ : pkey): unit option

  proc enc(_ : (pkey * pkey), _ : adata, _ : ptxt, _ : nonce) : ctxt option
  proc dec(_ : (pkey * pkey), _ : adata, _ : ctxt, _ : nonce) : ptxt option
}.

module type GMODPKAE_in_PKEY = {
  proc gen() : pkey option
  proc csetpk(_ : pkey) : unit option
}.

module type GMODPKAE_in_NIKE = {
  proc genssk(_ : pkey * pkey): unit option
}.

module type GMODPKAE_in_AE = {
  proc enc(_ : (pkey * pkey), _ : adata, _ : ptxt, _ : nonce) : ctxt option
  proc dec(_ : (pkey * pkey), _ : adata, _ : ctxt, _ : nonce) : ptxt option
}.

module GMODPKAE_in (PK : GMODPKAE_in_PKEY) (KE : GMODPKAE_in_NIKE) (E : GMODPKAE_in_AE) = {
  include PK
  include KE
  include E
}.

module GMODPKAE (O : GMODPKAE_in) : GPKAE_out = {
  proc gen    = O.gen
  proc csetpk = O.csetpk

  proc pkenc(pks, pkr, d, m, n) = {
    var v;
    var c <- None;

    v <@ O.genssk(pks, pkr);
    if (v <> None) {
      c <@ O.enc(sort pks pkr, d, m, n);
    }
    return c;
  }

  proc pkdec(pkr, pks, d, c, n) = {
    var v;
    var m <- None;

    v <@ O.genssk(pkr, pks);
    if (v <> None) {
      m <@ O.dec(sort pks pkr, d, c, n);
    }
    return m;
  }
}.

module GMODPKAE_mux (PK : GMODPKAE_in_PKEY) (KE : GMODPKAE_in_NIKE) (E : GMODPKAE_in_AE) =
  GMODPKAE(GMODPKAE_in(PK, KE, E)).

abstract theory PreSecurity.
require import PKEY.
require (*--*) KEY AE.

(** Instantiations **)
clone import PKEY as Pkey with
  type pkey <- pkey,
  type skey <- skey,
    op  dkp <- dkp
proof
  dkp_ll by exact: dkp_ll.

clone import KEY as Key with
  type handle <- pkey * pkey,
  type    key <- key,
  op     dkey <- dkey
proof *.

import Pkae.

clone import AE with
          type key    <- key,
          type adata  <- adata,
          type ptxt   <- ptxt,
          type ctxt   <- ctxt,
          type nonce  <- nonce,
          type handle <- pkey * pkey,
            op length <- length,
            op dctxt  <- dctxt,
  op Replication.dkey <- dkey
proof
  ge0_length by exact/ge0_length,
  *.

(** GMODPKAE as a reduction from GNIKE **)
module (R_GNIKE (E : NBSES) (G : GNIKE_out) : GPKAE_out) =
  GMODPKAE_mux(G, G, AE0(E, G)).

(** GMODPKAE as a reduction from GAE **)
module (R_GAE (G : GAE_out) : GPKAE_out) = GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, G), G).

(** The security proof **)
section Proof.
declare module E  <: NBSES   { -PKAE0, -PKAE1, -AE0, -AE1, -PKEY1, -KEYb }.
declare module A  <: D_GPKAE { -PKAE0, -PKAE1, -AE0, -AE1, -PKEY1, -KEYb, -E }.

(** Invariants **)
local clone import NIKE'.Invariant_mux as NIKE_Invariant with
      op dkey <- dkey,
  theory PKEY <- Pkey,
  theory  KEY <- Key.

local clone import AE.Invariant_mux as AE_Invariant with
      op dkey <- dkey,
   theory KEY <- Key.

(** Step 1 **)
local equiv Step1_gen:
  GPKAE0(Cryptobox(E), PKEY1).gen ~ GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY0), AE0(E, KEY0)).gen:
     ={glob E, glob PKEYb, arg}
  /\ ={log}(PKAEb, AEb)
  /\ (forall pks, inv_nike0 PKEYb.skm KEYb.keys pks){2}
  /\ (forall h, inv_ae KEYb.keys AEb.log h){2}
  ==>    ={glob E, glob PKEYb, res}
      /\ ={log}(PKAEb, AEb)
      /\ (forall pks, inv_nike0 PKEYb.skm KEYb.keys pks){2}
      /\ (forall h, inv_ae KEYb.keys AEb.log h){2}.
proof.
conseq (: ={glob E, glob PKEYb, res} /\ ={log}(PKAEb, AEb))
       _
       GNIKE0_inv_gen=> />.
by sim.
qed.

local equiv Step1_csetpk:
  GPKAE0(Cryptobox(E), PKEY1).csetpk ~ GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY0), AE0(E, KEY0)).csetpk :
     ={glob E, glob PKEYb, arg}
  /\ ={log}(PKAEb, AEb)
  /\ (forall pks, inv_nike0 PKEYb.skm KEYb.keys pks){2}
  /\ (forall h, inv_ae KEYb.keys AEb.log h){2}
  ==>    ={glob E, glob PKEYb, res}
      /\ ={log}(PKAEb, AEb)
      /\ (forall pks, inv_nike0 PKEYb.skm KEYb.keys pks){2}
      /\ (forall h, inv_ae KEYb.keys AEb.log h){2}.
proof.
conseq (: ={glob E, glob PKEYb, res} /\ ={log}(PKAEb, AEb))
       _
       GNIKE0_inv_csetpk=> />.
by sim.
qed.

local equiv eq0_genssk:
  GMODPKAE_in(PKEY1, NIKEb_mux(PKEY1, KEY0), AE0(E, KEY0)).genssk ~ GNIKEb_mux(PKEY1, KEY0).genssk:
    ={glob AEb, glob PKEYb, glob KEYb, arg} ==> ={glob AEb, glob PKEYb, glob KEYb, res}.
proof. by proc; sim. qed.

local equiv eq1_genssk:
  GMODPKAE_in(PKEY1, NIKEb_mux(PKEY1, KEY1), AE1(E, KEY1)).genssk ~ GNIKEb_mux(PKEY1, KEY1).genssk:
    ={glob AEb, glob PKEYb, glob KEYb, arg} ==> ={glob AEb, glob PKEYb, glob KEYb, res}.
proof. by proc; sim. qed.

local equiv Step1_pkenc:
  GPKAE0(Cryptobox(E), PKEY1).pkenc ~ GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY0), AE0(E, KEY0)).pkenc:
     ={glob E, glob PKEYb, arg}
  /\ ={log}(PKAEb, AEb)
  /\ (forall pks, inv_nike0 PKEYb.skm KEYb.keys pks){2}
  /\ (forall h, inv_ae KEYb.keys AEb.log h){2}
  ==>    ={glob E, glob PKEYb, res}
      /\ ={log}(PKAEb, AEb)
      /\ (forall pks, inv_nike0 PKEYb.skm KEYb.keys pks){2}
      /\ (forall h, inv_ae KEYb.keys AEb.log h){2}.
proof.
conseq (: ={glob E, glob PKEYb, res} /\ ={log}(PKAEb, AEb))
       _
       (:    (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk)
          /\ (forall h, inv_ae KEYb.keys AEb.log h)
          ==>    (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk)
              /\ (forall h, inv_ae KEYb.keys AEb.log h))=> //.
(** Hoare logic invariant **)
+ proc.
  seq  2: (#pre).
  + call (:    (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk)
            /\ (forall h, inv_ae KEYb.keys AEb.log h)
            ==>    (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk)
                /\ (forall h, inv_ae KEYb.keys AEb.log h)); last by auto.
    conseq eq0_genssk
           (:    (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk)
              /\ (forall h, inv_ae KEYb.keys AEb.log h)
              ==>    (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk)
                  /\ (forall h, inv_ae KEYb.keys AEb.log h))=> [/#|/#|].
    conseq GNIKE0_inv_genssk (: (forall h, inv_ae KEYb.keys AEb.log h)
                                ==> (forall h, inv_ae KEYb.keys AEb.log h))=> //.
    proc.
    inline 3; inline 2; sp; if=> //.
    inline 2; sp; if=> //.
    + by call (GAE0_K0_inv_set E); auto=> /#.
    by call (GAE0_K0_inv_cset E); auto=> /#.
  if=> //; conseq />.
  by call (GAE0_K0_inv_enc E).
(** Equivalence still needs to be done path-by-path
    because of abstract calls and samplings in control-flow **)
proc.
exists * PKEYb.skm.[pks]{1}, PKEYb.skm.[pkr]{1}.
elim *=> - [srro|srs [|srr]].
+ rcondf {1}  4; first by auto; inline *; auto=> &1 |> <-.
  inline NIKEb_mux(PKEY1, KEY0).genssk.
  rcondf {2}  7; first by auto; inline *; auto=> &2 |> <-.
  rcondf {2}  8; first by auto; inline *; auto.
  by inline *; auto.
+ rcondf {1} 4; first by auto; inline *; auto=> &1 |> <- <-.
  inline NIKEb_mux(PKEY1, KEY0).genssk.
  rcondf {2} 7; first by auto; inline *; auto=> &2 |> <- <-.
  rcondf {2} 8; first by auto; inline *; auto.
  by inline *; auto.
rcondt {1} 4; first by auto; inline *; auto=> &1 |> <- <-.
elim: srs=> [sks|]; last first.
+ inline NIKEb_mux(PKEY1, KEY0).genssk PKEY1.honpk PKEY1.getsk.
  rcondf {1}  9; first by auto=> /> <-.
  rcondf {1} 10; first by auto.
  rcondf {2}  9.
  + auto=> /> &0 /eq_sym skm_pks /eq_sym skm_pkr inv.
    by case: (inv pks{m}); rewrite skm_pks=> // ->.
  rcondf {2} 10; first by auto; inline *; auto.
  by auto.
rcondt {1} 6; first by auto; inline *; auto=> /#.
inline NIKEb_mux(PKEY1, KEY0).genssk PKEY1.honpk PKEY1.getsk.
rcondt {1}  9.
+ by auto=> /> <-.
rcondt {2}  9.
+ by auto=> /> &0 <- <-.
rcondt {2} 12.
+ by auto=> /> &0 <-.
inline AE0(E, KEY0).enc KEY0.get KEY0.hon.
exists * PKAEb.log.[sort pks pkr, n]{1}; elim * => - [|[] m' c']; last first.
+ rcondf {1} 11; first by auto=> /#.
  rcondt {2} 17; first by auto; inline *; auto.
  rcondf {2} 24.
  + by auto; inline *; auto=> /> &0 <-.
  by inline *; auto.
rcondt {1} 11; first by auto=> /#.
rcondt {2} 17; first by auto; inline *; auto.
rcondt {2} 24.
+ by auto; inline *; auto=> /> &0 <- //=; rewrite !get_set_sameE.
inline Cryptobox(E).enc.
auto; call (: true); inline *; auto=> /> &0.
move=> _ /eq_sym ^ skm_pks -> @/get_as_Honest @/get_as_Some //=; rewrite !get_set_sameE //=.
move=> /eq_sym skm_pkr; rewrite skm_pkr //=.
move=> inv_nike inv_ae.
case: (inv_nike pks{0}); rewrite skm_pks=> /> kps_in_dkp keys_pks_X.
by have [] ->:= keys_pks_X _ _ skm_pkr.
qed.

local equiv Step1_pkdec:
  GPKAE0(Cryptobox(E), PKEY1).pkdec ~ GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY0), AE0(E, KEY0)).pkdec:
     ={glob E, glob PKEYb, arg}
  /\ ={log}(PKAEb, AEb)
  /\ (forall pks, inv_nike0 PKEYb.skm KEYb.keys pks){2}
  /\ (forall h, inv_ae KEYb.keys AEb.log h){2}
  ==>    ={glob E, glob PKEYb, res}
      /\ ={log}(PKAEb, AEb)
      /\ (forall pks, inv_nike0 PKEYb.skm KEYb.keys pks){2}
      /\ (forall h, inv_ae KEYb.keys AEb.log h){2}.
proof.
conseq (: ={glob E, glob PKEYb, res} /\ ={log}(PKAEb, AEb))
       _
       (:    (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk)
          /\ (forall h, inv_ae KEYb.keys AEb.log h)
          ==>    (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk)
              /\ (forall h, inv_ae KEYb.keys AEb.log h))=> //.
+ proc.
  seq  2: #pre.
  + call (:    (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk)
            /\ (forall h, inv_ae KEYb.keys AEb.log h)
            ==>    (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk)
                /\ (forall h, inv_ae KEYb.keys AEb.log h)); last by auto.
    conseq eq0_genssk
           (:    (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk)
              /\ (forall h, inv_ae KEYb.keys AEb.log h)
              ==>    (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk)
                  /\ (forall h, inv_ae KEYb.keys AEb.log h))=> [/#|/#|].
    conseq GNIKE0_inv_genssk (: (forall h, inv_ae KEYb.keys AEb.log h) ==> (forall h, inv_ae KEYb.keys AEb.log h))=> //.
    proc; inline 3; inline 2; sp; if=> //.
    inline 2; sp; if=> //.
    + by call (GAE0_K0_inv_set E); auto=> /#.
    by call (GAE0_K0_inv_cset E); auto=> /#.
  by conseq />.
proc.
exists * PKEYb.skm.[pks]{1}, PKEYb.skm.[pkr]{1}.
elim *=> - [srro|srs [|srr]].
+ rcondf {1} 4; first by auto; inline *; auto=> &1 |> <-.
  inline NIKEb_mux(PKEY1, KEY0).genssk.
  rcondf {2} 7; first by auto; inline *; auto=> /> &2 <-.
  rcondf {2} 8; first by auto; inline *; auto.
  by inline *; auto.
+ rcondf {1} 4; first by auto; inline *; auto=> &1 |> <- <-.
  inline NIKEb_mux(PKEY1, KEY0).genssk.
  rcondf {2} 7; first by auto; inline *; auto=> &2 |> <- <-.
  rcondf {2} 8; first by auto; inline *; auto.
  by inline *; auto.
rcondt {1} 4; first by auto; inline *; auto=> &1 |> <- <-.
inline NIKEb_mux(PKEY1, KEY0).genssk PKEY1.honpk PKEY1.getsk.
elim: srr=> [skr|]; last first.
+ inline NIKEb_mux(PKEY1, KEY0).genssk PKEY1.honpk PKEY1.getsk.
  rcondf {1} 8; first by auto=> /> &1 <-.
  rcondf {1} 9; first by auto=> /> &1 <-.
  rcondf {2}  9; first by auto=> /> &0 <- <-.
  rcondf {2} 10; first by auto.
  by auto.
rcondt {1} 8; first by auto=> /> &1 <-.
rcondt {1} 10; first by auto=> /#.
rcondt {2}  9; first by auto=> /> &0 <- <-.
rcondt {2} 17; first by auto; inline *; auto.
inline NIKEb_mux(PKEY1, KEY0).genssk PKEY1.honpk PKEY1.getsk AE0(E, KEY0).dec KEY0.get KEY0.hon.
rcondt {2} 24.
+ auto; inline *; auto=> /> &0 /eq_sym skm_pks /eq_sym skm_pkr inv_nike inv_ae.
  have //= ->:= sort_is_permutation pks{m} pkr{m} pkr{m} pks{m}.
  by rewrite skm_pks !get_set_sameE.
inline *; auto; call (: true).
auto=> /> &0 /eq_sym skm_pks /eq_sym skm_pkr inv_nike inv_ae.
have //= ->:= sort_is_permutation pks{0} pkr{0} pkr{0} pks{0}.
rewrite !get_set_sameE skm_pkr skm_pks /get_as_Some=> //=.
case: (inv_nike pkr{0}); rewrite skm_pkr=> /> kpr_in_dkp keys_pkr_X.
by have [] ->:= keys_pkr_X _ _ skm_pks.
qed.

local equiv Step1_eq: A(GPKAE0(Cryptobox(E), PKEY1)).run ~ A(GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY0), AE0(E, KEY0))).run:
     ={glob A, glob E, glob PKEYb, arg}
  /\ ={log}(PKAEb, AEb)
  /\ (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk){2}
  /\ (forall h, inv_ae KEYb.keys AEb.log h){2}
  ==> ={res}.
proof.
proc (   ={glob E, glob PKEYb}
      /\ ={log}(PKAEb, AEb)
      /\ (forall pk, inv_nike0 PKEYb.skm KEYb.keys pk){2}
      /\ (forall h, inv_ae KEYb.keys AEb.log h){2})=> //.
+ by conseq Step1_pkenc=> /#.
+ by conseq Step1_pkdec=> /#.
+ by conseq Step1_gen.
by conseq Step1_csetpk.
qed.

(** Step 2 **)
local equiv Step2_eq (K <: KEY_out { -A, -E, -AEb, -PKEYb }) :
    A(GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, K), AE0(E, K))).run
  ~ A(R_GNIKE(E, GNIKEb_mux(PKEY1, K))).run:
      ={glob A, glob E, glob K, glob AEb, glob PKEYb}
      ==> ={res}.
proof. by sim. qed.

(** Step 3 **)
local equiv Step3_eq (AE <: AE { -A, -E, -KEYb, -PKEYb }) :
    A(GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY1), AE(E, KEY1))).run
  ~ A(R_GAE(GAEb(E, KEY1, AE))).run:
      ={glob A, glob E, glob AE, glob KEYb, glob PKEYb}
      ==> ={res}.
proof. by sim. qed.

(** Step 4 **)
local equiv Step4_gen:
  GPKAE1(Cryptobox(E), PKEY1).gen ~ GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY1), AE1(E, KEY1)).gen:
     ={glob E, glob PKEYb, arg}
  /\ ={log}(PKAEb, AEb)
  /\ (forall pks, inv_nike1 PKEYb.skm KEYb.keys pks){2}
  ==>    ={glob E, glob PKEYb, res}
      /\ ={log}(PKAEb, AEb)
      /\ (forall pks, inv_nike1 PKEYb.skm KEYb.keys pks){2}.
proof.
conseq (: ={glob E, glob PKEYb, res} /\ ={log}(PKAEb, AEb))
       _
       GNIKE1_inv_gen=> //.
by sim.
qed.

local equiv Step4_csetpk:
  GPKAE1(Cryptobox(E), PKEY1).csetpk ~ GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY0), AE1(E, KEY1)).csetpk:
     ={glob E, glob PKEYb, arg}
  /\ ={log}(PKAEb, AEb)
  /\ (forall pks, inv_nike1 PKEYb.skm KEYb.keys pks){2}
  ==>    ={glob E, glob PKEYb, res}
      /\ ={log}(PKAEb, AEb)
      /\ (forall pks, inv_nike1 PKEYb.skm KEYb.keys pks){2}.
proof.
conseq (: ={glob E, glob PKEYb, res} /\ ={log}(PKAEb, AEb))
       _
       GNIKE1_inv_csetpk=> //.
by sim.
qed.

local equiv Step4_pkenc:
  GPKAE1(Cryptobox(E), PKEY1).pkenc ~ GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY1), AE1(E, KEY1)).pkenc:
     ={glob E, glob PKEYb, arg}
  /\ ={log}(PKAEb, AEb)
  /\ (forall pks, inv_nike1 PKEYb.skm KEYb.keys pks){2}
  ==>    ={glob E, glob PKEYb, res}
      /\ ={log}(PKAEb, AEb)
      /\ (forall pks, inv_nike1 PKEYb.skm KEYb.keys pks){2}.
proof.
conseq (: ={glob E, glob PKEYb, res} /\ ={log}(PKAEb, AEb))
       _
       (: (forall pk, inv_nike1 PKEYb.skm KEYb.keys pk)
          ==> (forall pk, inv_nike1 PKEYb.skm KEYb.keys pk))=> //.
+ proc.
  seq 2: #pre.
  + call (: (forall pk, inv_nike1 PKEYb.skm KEYb.keys pk)
            ==> (forall pk, inv_nike1 PKEYb.skm KEYb.keys pk)); last by auto.
    by conseq eq1_genssk GNIKE1_inv_genssk=> /#.
  by conseq />.
proc.
exists * PKEYb.skm.[pks]{1}, PKEYb.skm.[pkr]{1}.
elim *=> - [srro|srs [|srr]].
+ rcondf {1} 4; first by auto; inline *; auto=> &1 |> <-.
  inline NIKEb_mux(PKEY1, KEY1).genssk.
  rcondf {2} 7; first by auto; inline *; auto=> &2 |> <-.
  rcondf {2} 8; first by auto; inline *; auto.
  by inline *; auto.
+ rcondf {1} 4; first by auto; inline *; auto=> &1 |> <- <-.
  inline NIKEb_mux(PKEY1, KEY1).genssk.
  rcondf {2} 7; first by auto; inline *; auto=> &2 |> <- <-.
  rcondf {2} 8; first by auto; inline *; auto.
  by inline *; auto.
rcondt {1} 4; first by auto; inline *; auto=> &1 |> <- <-.
elim: srs=> [sks|]; last first.
+ inline NIKEb_mux(PKEY1, KEY1).genssk PKEY1.honpk PKEY1.getsk.
  rcondf {1}  9; first by auto=> /> <-.
  rcondf {1} 10; first by auto=> /> &0 <-.
  rcondf {2}  9; first by auto=> /> &0 <-.
  rcondf {2} 10; first by auto.
  by auto.
inline NIKEb_mux(PKEY1, KEY1).genssk PKEY1.honpk PKEY1.getsk.
rcondt {1}  9; first by auto=> /> <-.
rcondt {1} 11; first by auto; auto=> /#.
rcondt {2}  9; first by auto=> /> &0 <- <-.
rcondt {2} 12; first by auto=> /> &0 <-.
inline AE1(E, KEY1).enc KEY1.get KEY1.hon.
rcondt {2} 17; first by auto; inline *; sp; if; auto.
exists * PKAEb.log.[sort pks pkr, n]{1}; elim * => - [|[] m' c']; last first.
+ rcondf {1} 11; first by auto=> /#.
  rcondf {2} 24.
  + by auto; inline *; sp; if; auto; smt(get_setE).
  by inline *; sp; if{2}; auto=> />; rewrite ?dkey_ll.
rcondt {1} 11; first by auto=> /#.
rcondt {2} 24.
+ by auto; inline *; sp; if; auto; smt(get_setE).
case @[ambient]: srr=> [skr|].
+ rcondt {1} 11; first by auto=> /#.
  rcondt {2} 15; first by auto=> /#.
  rcondt {2} 27.
  + auto; inline *; auto=> /> &0 + + + + k _; rewrite get_set_sameE //=.
    move=> _ /eq_sym skm_pks /eq_sym skm_pkr /(_ pks{m}) []; rewrite skm_pks=> />.
    by move=> _ _ /(_ _ _ skm_pkr) [|[] k'] ->.
  by inline *; auto=> /> &0; rewrite dkey_ll.
rcondf {1} 11; first by auto=> /#.
rcondf {2} 15; first by auto=> /#.
rcondf {2} 27.
+ auto; inline *; auto=> /> &0; rewrite get_set_sameE /=.
  move=> _ /eq_sym skm_pks /eq_sym skm_pkr inv_nike.
  rewrite skm_pks /get_as_Some /get_as_Honest /=.
  by case: (inv_nike pks{m}); rewrite skm_pks=> /> _ /(_ _ skm_pkr) [] ->.
inline *; auto; call (: true); auto=> />.
move=> &0 /eq_sym log_hn /eq_sym skm_pks /eq_sym skm_pkr inv_nike.
rewrite !get_set_sameE skm_pks /get_as_Some /get_as_Honest=> //=.
case: (inv_nike pks{0}); rewrite skm_pks=> /> kps_in_dkp keys_pks_C keys_pks_H.
by have [] ->:= keys_pks_C _ skm_pkr.
qed.

local equiv Step4_pkdec:
  GPKAE1(Cryptobox(E), PKEY1).pkdec ~ GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY1), AE1(E, KEY1)).pkdec:
     ={glob E, glob PKEYb, arg}
  /\ ={log}(PKAEb, AEb)
  /\ (forall pks, inv_nike1 PKEYb.skm KEYb.keys pks){2}
  ==>    ={glob E, glob PKEYb, res}
      /\ ={log}(PKAEb, AEb)
      /\ (forall pks, inv_nike1 PKEYb.skm KEYb.keys pks){2}.
proof.
conseq (: ={glob E, glob PKEYb, res} /\ ={log}(PKAEb, AEb))
       _
       (: (forall pk, inv_nike1 PKEYb.skm KEYb.keys pk)
          ==> (forall pk, inv_nike1 PKEYb.skm KEYb.keys pk))=> //.
+ proc.
  seq 2: #pre.
  + call (: (forall pk, inv_nike1 PKEYb.skm KEYb.keys pk)
            ==> (forall pk, inv_nike1 PKEYb.skm KEYb.keys pk)); last by auto.
    by conseq eq1_genssk GNIKE1_inv_genssk=> /#.
  by conseq />.
proc.
exists * PKEYb.skm.[pks]{1}, PKEYb.skm.[pkr]{1}.
elim *=> - [srro|srs [|srr]].
+ rcondf {1} 4; first by auto; inline *; auto=> &1 |> <-.
  inline NIKEb_mux(PKEY1, KEY1).genssk.
  rcondf {2} 7; first by auto; inline *; auto=> &2 |> <-.
  rcondf {2} 8; first by auto; inline *; auto.
  by inline *; auto.
+ rcondf {1} 4; first by auto; inline *; auto=> &1 |> <- <-.
  inline NIKEb_mux(PKEY1, KEY1).genssk.
  rcondf {2} 7; first by auto; inline *; auto=> &2 |> <- <-.
  rcondf {2} 8; first by auto; inline *; auto.
  by inline *; auto.
rcondt {1} 4; first by auto; inline *; auto=> &1 |> <- <-.
elim: srr=> [skr|]; last first.
+ inline NIKEb_mux(PKEY1, KEY1).genssk PKEY1.honpk PKEY1.getsk.
  rcondf {1}  8; first by auto=> /> &0 <-.
  rcondf {1}  9; first by auto=> /> &0 <-.
  rcondf {2}  9; first by auto=> /> &0 <- <-.
  rcondf {2} 10; first by auto.
  by auto.
inline NIKEb_mux(PKEY1, KEY1).genssk PKEY1.honpk PKEY1.getsk.
rcondt {1}  8; first by auto=> /> _ <-.
rcondt {1} 10; first by auto=> /#.
rcondt {2}  9; first by auto=> /> &0 <- <-.
rcondt {2} 12; first by auto=> /> &0 _ <-.
rcondt {2} 17; first by auto; inline *; sp; if; auto.
inline AE1(E, KEY1).dec KEY1.get KEY1.hon.
rcondt {2} 24.
+ auto; inline *; sp; if; auto=> />.
  + move=> &0 _ _ _ _ k _.
    have //= ->:= sort_is_permutation pks{m} pkr{m} pkr{m} pks{m}.
    by rewrite get_set_sameE.
  move=> &0.
  have //= ->:= sort_is_permutation pks{m} pkr{m} pkr{m} pks{m}.
  by rewrite get_set_sameE.
elim: srs=> [sks|].
+ rcondt {1} 11; first by auto=> /#.
  rcondt {2} 15; first by auto=> /#.
  rcondt {2} 27.
  + auto; inline *; auto=> /> &0 /eq_sym skm_pks /eq_sym skm_pkr inv_nike k _.
    have //= -> := sort_is_permutation pkr{m} pks{m} pks{m} pkr{m}.
    rewrite get_set_sameE //=.
    by case: (inv_nike pks{m}); rewrite skm_pks=> /> _ _ /(_ _ _ skm_pkr) [|[] k'] ->.
  by inline *; auto=> /> &2; rewrite dkey_ll.
rcondf {1} 11; first by auto=> /#.
rcondf {2} 15; first by auto=> /#.
rcondf {2} 27.
+ auto; inline *; auto=> /> &0 /eq_sym skm_pks /eq_sym skm_pkr inv_nike.
  have //= ->:= sort_is_permutation pks{m} pkr{m} pkr{m} pks{m}.
  rewrite get_set_sameE skm_pkr /get_as_Honest //=.
  by case: (inv_nike pkr{m}); rewrite skm_pkr=> /> _ /(_ _ skm_pks) [] ->.
inline *; auto; call (: true).
auto=> |> &0 /eq_sym skm_pks /eq_sym skm_pkr inv_nike.
have //= ->:= sort_is_permutation pks{0} pkr{0} pkr{0} pks{0}.
rewrite !get_set_sameE skm_pkr /get_as_Some /get_as_Honest=> //=.
case: (inv_nike pkr{0}); rewrite skm_pkr=> /> kpr_in_dkp keys_pkr_C keys_pkr_H.
by have [] ->:= keys_pkr_C _ skm_pks.
qed.

local equiv Step4_eq:
  A(GPKAE1(Cryptobox(E), PKEY1)).run ~ A(GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY1), AE1(E, KEY1))).run:
     ={glob A, glob E, glob PKEYb, arg}
  /\ ={log}(PKAEb, AEb)
  /\ (forall pk, inv_nike1 PKEYb.skm KEYb.keys pk){2}
  ==> ={res}.
proof.
proc (   ={glob E, glob PKEYb}
      /\ ={log}(PKAEb, AEb)
      /\ (forall pk, inv_nike1 PKEYb.skm KEYb.keys pk){2})=> //.
+ by conseq Step4_pkenc=> /#.
+ by conseq Step4_pkdec=> /#.
+ by conseq Step4_gen.
by conseq Step4_csetpk.
qed.

(** This violates package boundaries!
    It is necessary so we can make it minimal and avoid double initialization.
    Top-level theorems should not use it (instead, quantify over
    initial memories with restricted initial values for a minimal set
    of variables), but we need to express intermediate theorems with
    it.
**)
module type Runner = {
  proc run(): bool
}.

module Init (R : Runner) = {
  proc run() = {
    var r;

    PKEYb.skm <- empty;
    PKAEb.log <- empty;
      AEb.log <- empty;
    KEYb.keys <- empty;
            r <@ R.run();
    return r;
  }
}.

lemma PreSecurity_Init &m:
     `|  Pr[Init(A(GPKAE0(Cryptobox(E), PKEY1))).run() @ &m : res]
       - Pr[Init(A(GPKAE1(Cryptobox(E), PKEY1))).run() @ &m : res]|
  <=   `|  Pr[Init(A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY0)))).run() @ &m: res]
         - Pr[Init(A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY1)))).run() @ &m: res]|
     + `|  Pr[Init(A(R_GAE(GAE0(E, KEY1)))).run() @ &m: res]
         - Pr[Init(A(R_GAE(GAE1(E, KEY1)))).run() @ &m: res]|.
proof.
have ->:   Pr[Init(A(GPKAE0(Cryptobox(E), PKEY1))).run() @ &m : res]
         = Pr[Init(A(GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY0), AE0(E, KEY0)))).run () @ &m: res].
+ byequiv=> //; proc; call Step1_eq; auto=> />; split=> [pk|h].
  + by apply: NIKE0_NotIn=> [|pkr]; exact: emptyE.
  by apply: AENotIn=> [|n]; exact: emptyE.
have ->:   Pr[Init(A(GPKAE1(Cryptobox(E), PKEY1))).run() @ &m : res]
         = Pr[Init(A(GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY1), AE1(E, KEY1)))).run () @ &m: res].
+ byequiv=> //; proc; call Step4_eq; auto=> /> pk.
  by apply: NIKE1_NotIn=> [|pkr]; exact: emptyE.
have <-:   Pr[Init(A(GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY0), AE0(E, KEY0)))).run() @ &m: res]
         = Pr[Init(A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY0)))).run() @ &m: res].
+ by byequiv=> //; proc; call (Step2_eq KEY0); auto.
have <-:   Pr[Init(A(GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY1), AE0(E, KEY1)))).run() @ &m: res]
         = Pr[Init(A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY1)))).run() @ &m: res].
+ by byequiv=> //; proc; call (Step2_eq KEY1); auto.
have <-:   Pr[Init(A(GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY1), AE0(E, KEY1)))).run() @ &m: res]
         = Pr[Init(A(R_GAE(GAE0(E, KEY1)))).run() @ &m : res].
+ by byequiv=> //; proc; call (Step3_eq AE0); auto.
have <-:   Pr[Init(A(GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, KEY1), AE1(E, KEY1)))).run() @ &m: res]
         = Pr[Init(A(R_GAE(GAE1(E, KEY1)))).run() @ &m : res].
+ by byequiv=> //; proc; call (Step3_eq AE1); auto.
smt().
qed.

lemma PreSecurity &m:
     PKEYb.skm{m}  = empty
  => PKAEb.log{m} = empty
  => AEb.log{m} = empty
  => KEYb.keys{m} = empty
  =>    `|  Pr[A(GPKAE0(Cryptobox(E), PKEY1)).run() @ &m : res]
          - Pr[A(GPKAE1(Cryptobox(E), PKEY1)).run() @ &m : res]|
     <=   `|  Pr[A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY0))).run() @ &m: res]
            - Pr[A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY1))).run() @ &m: res]|
        + `|  Pr[A(R_GAE(GAE0(E, KEY1))).run() @ &m: res]
            - Pr[A(R_GAE(GAE1(E, KEY1))).run() @ &m: res]|.
proof.
move=> pkey_skm pkae_log ae_log k_keys.
have ->:   Pr[A(GPKAE0(Cryptobox(E), PKEY1)).run() @ &m: res]
         = Pr[Init(A(GPKAE0(Cryptobox(E), PKEY1))).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
have ->:   Pr[A(GPKAE1(Cryptobox(E), PKEY1)).run() @ &m: res]
         = Pr[Init(A(GPKAE1(Cryptobox(E), PKEY1))).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
have ->:   Pr[A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY0))).run() @ &m: res]
         = Pr[Init(A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY0)))).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
have ->:   Pr[A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY1))).run() @ &m: res]
         = Pr[Init(A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY1)))).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
have ->:   Pr[A(R_GAE(GAE0(E, KEY1))).run() @ &m: res]
         = Pr[Init(A(R_GAE(GAE0(E, KEY1)))).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
have ->:   Pr[A(R_GAE(GAE1(E, KEY1))).run() @ &m: res]
         = Pr[Init(A(R_GAE(GAE1(E, KEY1)))).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
exact: (PreSecurity_Init &m).
qed.
end section Proof.
end PreSecurity.

theory Security.
require import PKEY.
require (*--*) SAE.
(*---*) export StdBigop.Bigreal.BRA.

(** Instantiations **)
clone import PKEY as Pkey with
  type pkey <- pkey,
  type skey <- skey,
    op  dkp <- dkp.

(** We use this to express the GNIKE reduction **)
clone import KEY as Key with
  type handle <- pkey * pkey,
  type    key <- key,
    op   dkey <- dkey
proof *.

clone import SAE with
  type    key <- key,
  type  adata <- adata,
  type   ptxt <- ptxt,
  type   ctxt <- ctxt,
  type  nonce <- nonce,
    op length <- length,
    op  dctxt <- dctxt,
    op   dkey <- dkey
proof
  ge0_length by exact/ge0_length,
  *.

(** GMODPKAE with a built-in AE0 as a reduction from GNIKE **)
module (R_GNIKE (E : NBSES) (G : GNIKE_out) : GPKAE_out) = {
  var log: ((pkey * pkey) * nonce, ptxt * (ctxt * adata)) fmap

  module AE0 = {
    proc enc(h, d, m, n) = {
      var ko, k, c;
      var co <- None;

      ko <@ G.get(h);
      if (ko <> None /\ log.[h, n] = None) {
                   k <- oget ko;
                   c <@ E.enc(d, m, n, k);
                  co <- Some c;
        log.[(h, n)] <- (m, (c, d));
      }
      return co;
    }
  
    proc dec(h, d, c, n) = {
      var ko, k;
      var m <- None;
  
      ko <@ G.get(h);
      if (ko <> None) {
        k <- oget ko;
        m <@ E.dec(d, c, n, k);
      }
      return m;
    }   
  }

  include GMODPKAE_mux(G, G, AE0)
}.

(** SAE Security: our Hybrid reduction **)
module R_GSAE (E : NBSES) (G : GSAE) : GPKAE_out = {
  var i    : int

  var hs   : (pkey * pkey, int) fmap
  var c    : int

  var keys : (pkey * pkey, bool * key) fmap
  var log  : ((pkey * pkey) * nonce, ptxt * (ctxt * adata)) fmap

  proc enc0(h, d, p, n) = {
    var c;
    var co <- None;

    if (h \in keys /\ log.[h, n] = None) {
                 c <@ E.enc(d, p, n, oget (omap snd keys.[h]));
                co <- Some c;
      log.[(h, n)] <- (p, (c, d));
    }
    return co;
  }

  proc enc1(h, d, p, n) = {
    var c;
    var co <- None;

    if (h \in keys /\ log.[h, n] = None) {
      if (odflt false (omap fst keys.[h])) {
        c <$ dctxt (length p);
      } else {
        c <@ E.enc(d, p, n, oget (omap snd keys.[h]));
      }
                co <- Some c;
      log.[(h, n)] <- (p, (c, d));
    }
    return co;
  }

  proc dec0(h, d, c, n) = {
    var m <- None;

    if (h \in keys) {
      m <@ E.dec(d, c, n, oget (omap snd keys.[h]));
    }
    return m;
  }

  proc dec1(h, d, c, n) = {
    var m <- None;

    if (h \in keys) {
      if (odflt false (omap fst keys.[h])) {
        m <- Pkae.fieso (c, d) log.[h, n];
      } else {
        m <@ E.dec(d, c, n, oget (omap snd keys.[h]));
      }
    }
    return m;
  }

  module Hybrid = {
    proc set(h, k) = {
      if (h \notin keys /\ h \notin hs) {
        if (c = i) {
          G(E).gen();
        }
        hs .[h] <- c;
              c <- c + 1;
      }
      k <$ dkey;
      keys.[h] <- odflt (true, k) keys.[h];
    }

    proc cset(h, k) = {
      keys.[h] <- odflt (false, k) keys.[h];
    }

    proc enc(h, d, p, n) = {
      var r <- None;

      if (h \in hs /\ oget hs.[h] < i) {
        r <@ enc1(h, d, p, n);
      } elif (h \in hs /\ oget hs.[h] = i) {
        r <@ G(E).enc(d, p, n);
      } else {
        r <@ enc0(h, d, p, n);
      }
      return r;
    }

    proc dec(h, d, c, n) = {
      var r <- None;

      if (h \in hs /\ oget hs.[h] < i) {
        r <@ dec1(h, d, c, n);
      } elif (h \in hs /\ oget hs.[h] = i) {
        r <@ G(E).dec(d, c, n);
      } else {
        r <@ dec0(h, d, c, n);
      }
      return r;
    }
  }

  include GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, Hybrid), Hybrid)
}.

module A_i (A : D_GPKAE) (G : GPKAE_out) = {
  proc run_i(j) = {
    var r;

    R_GSAE.i <- j;
           r <@ A(G).run();
    return r;
  }
}.

(** Counting oracle queries **)    
module Count (G : GPKAE_out) = {
  var q_gen    : int
  var q_csetpk : int
  var q_pkenc  : int
  var q_pkdec  : int

  proc gen() = {
    var r;

        r <@ G.gen();
    q_gen <- q_gen + 1;
    return r;
  }

  proc csetpk(pk) = {
    var r;

           r <@ G.csetpk(pk);
    q_csetpk <- q_csetpk + 1;
    return r;
  }

  proc pkenc(pks, pkr, d, m, n) = {
    var r;

          r <@ G.pkenc(pks, pkr, d, m, n);
    q_pkenc <- q_pkenc + 1;
    return r;
  }

  proc pkdec(pkr, pks, d, c, n) = {
    var r;

          r <@ G.pkdec(pkr, pks, d, c, n);
    q_pkdec <- q_pkdec + 1;
    return r;
  }
}.

equiv Count_PKAE (G <: GPKAE_out { -Count }) (D <: D_GPKAE { -Count, -G }):
  D(G).run ~ D(Count(G)).run: ={glob G, glob D} ==> ={res}.
proof.
proc (={glob G})=> />.
+ by proc *; inline Count(G).pkenc; auto; call (: true); auto.
+ by proc *; inline Count(G).pkdec; auto; call (: true); auto.
+ by proc *; inline Count(G).gen; auto; sim.
by proc *; inline Count(G).csetpk; auto; call (: true); auto.
qed.

section Proof.
declare op q_gen_max    : { int | 0 <= q_gen_max    } as ge0_q_gen_max.
declare op q_csetpk_max : { int | 0 <= q_csetpk_max } as ge0_q_csetpk_max.
declare op q_pkenc_max  : { int | 0 <= q_pkenc_max  } as ge0_q_pkenc_max.
declare op q_pkdec_max  : { int | 0 <= q_pkdec_max  } as ge0_q_pkdec_max.

declare op pr_guess     : { real | forall pk, mu dkp (fun (x: _ * _) => x.`1 = pk) <= pr_guess } as pr_guessP.

declare module E <: NBSES   { -PKAE0, -PKAE1, -R_GNIKE, -R_GSAE, -KEYb, -PKEYb, -SAEb, -Count }.
declare module A <: D_GPKAE { -PKAE0, -PKAE1, -R_GNIKE, -R_GSAE, -KEYb, -PKEYb, -SAEb, -Count, -E }.

declare axiom E_enc_ll: islossless E.enc.
declare axiom E_dec_ll: islossless E.dec.

declare axiom A_ll (G <: GPKAE_out { -A }):
     islossless G.pkenc
  => islossless G.pkdec
  => islossless G.gen
  => islossless G.csetpk
  => islossless A(G).run.

declare axiom A_count c_gen c_csetpk c_pkenc c_pkdec (G <: GPKAE_out { -A, -Count }):
  hoare [A(Count(G)).run:
              Count.q_gen = c_gen
           /\ Count.q_csetpk = c_csetpk
           /\ Count.q_pkenc = c_pkenc
           /\ Count.q_pkdec = c_pkdec
           ==>    Count.q_gen <= c_gen + q_gen_max
               /\ Count.q_csetpk <= c_csetpk + q_csetpk_max
               /\ Count.q_pkenc <= c_pkenc + q_pkenc_max
               /\ Count.q_pkdec <= c_pkdec + q_pkdec_max].

(** There's more glue than material in this proof... **)
(** Locally cloning the "trunk" lemma **)
local clone PreSecurity with
  theory Pkey <- Pkey,
  theory  Key <- Key
proof *.

(** Locally cloning "branch" lemmas **)
local clone Switching_0_1 as Switching
proof *.

local clone PreSecurity.AE.Replication with
  theory KEY <- Key,
  theory SAE <- SAE
proof
  dkey_ll by exact: dkey_ll,
  dctxt_ll by exact: dctxt_ll,
  *.

(** Transferring Counters: Switching to "real" collision-ey PKEY **)
local module D0 (K : PKEY_out) = A(Count(GPKAE0(Cryptobox(E), K))).

local equiv Count_Count0 (K <: PKEY_out { -D0, -Switching.Counting.Count }):
   D0(Switching.Counting.Count(K)).run ~ D0(K).run:
       ={glob A, glob K, glob E, glob PKAEb}
    /\ ={q_gen, q_csetpk}(Switching.Counting.Count, Count)
    ==>    ={glob A, glob K, glob E, glob PKAEb, res}
        /\ ={q_gen, q_csetpk}(Switching.Counting.Count, Count).
proof.
proc (   ={glob K, glob E, glob PKAEb}
      /\ ={q_gen, q_csetpk}(Switching.Counting.Count, Count))=> //; first 2 by sim.
+ by proc; inline *; sim.
by proc; inline *; sim.
qed.

local module D1 (K : PKEY_out) = A(Count(GPKAE1(Cryptobox(E), K))).

local equiv Count_Count1 (K <: PKEY_out { -D1, -Switching.Counting.Count }):
   D1(Switching.Counting.Count(K)).run ~ D1(K).run:
       ={glob A, glob K, glob E, glob PKAEb}
    /\ ={q_gen, q_csetpk}(Switching.Counting.Count, Count)
    ==>    ={glob A, glob K, glob E, glob PKAEb, res}
        /\ ={q_gen, q_csetpk}(Switching.Counting.Count, Count).
proof.
proc (   ={glob K, glob E, glob PKAEb}
      /\ ={q_gen, q_csetpk}(Switching.Counting.Count, Count))=> //; first 2 by sim.
+ by proc; inline *; sim.
by proc; inline *; sim.
qed.

(** Transferring Counters: Hybrid reduction from single-instance AE **)
local module D' (PK : PKEY) (G : PreSecurity.AE.GAE_out) =
  A(Count(GMODPKAE_mux(PK, NIKEb_mux(PK, G), G))).

local equiv Count_Count0' (G <: PreSecurity.AE.GAE_out { -D', -PKEY1, -Replication.Count })
            (c c_pkenc c_pkdec : int):
  D'(PKEY1, Replication.Count(G)).run ~ D'(PKEY1, G).run:
       ={glob A, glob D'(PKEY1, G)}
    /\ Replication.Count.c{1} = c
    /\ Count.q_pkenc{2} = c_pkenc
    /\ Count.q_pkdec{2} = c_pkdec
    ==>    ={glob A, glob D'(PKEY1, G)}
        /\ Replication.Count.c{1} <= c + (Count.q_pkenc{2} - c_pkenc) + (Count.q_pkdec{2} - c_pkdec).
proof.
conseq (: ={glob A, glob D'(PKEY1, G)})
       (:    Replication.Count.c = c
          /\ Count.q_pkenc = c_pkenc
          /\ Count.q_pkdec = c_pkdec
          ==> Replication.Count.c <= c + (Count.q_pkenc - c_pkenc) + (Count.q_pkdec - c_pkdec))=> //.
+ proc (   c <= Replication.Count.c <= c + (Count.q_pkenc - c_pkenc) + (Count.q_pkdec - c_pkdec)
        /\ c_pkenc <= Count.q_pkenc
        /\ c_pkdec <= Count.q_pkdec)=> //.
  + proc; inline *; sp; if; auto.
    + sp; if; auto.
      + by swap 4 2; wp; conseq (: true)=> // /#.
      by conseq (: true)=> // /#.
    by conseq (: true)=> // /#.
  + proc; inline *; sp; if; auto.
    + sp; if; auto.
      + by swap 4 2; wp; conseq (: true)=> // /#.
      by conseq (: true)=> // /#.
    by conseq (: true)=> // /#.
  + by conseq (: true).
  by conseq (: true).
proc ( ={glob Count(GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, G), G))})=> //.
+ proc; wp=> //=.
  conseq (: ={r, glob G})=> //.
  inline *; sp; if=> //; last by sim=> /#.
  sp; if=> // [/#| |]; last by sim=> /> /#.
  sp; seq  1  1: #pre; first by call (: true); auto=> /#.
  by sim=> /#.
+ proc; wp=> //=.
  conseq (: ={r, glob G})=> //.
  inline *; sp; if=> //; last by sim=> /#.
  sp; if=> // [/#| |]; last by sim=> /> /#.
  sp; seq  1  1: #pre; first by call (: true); auto=> /#.
  by sim=> /#.
+ by sim.
by sim.
qed.

(** Crafting an initial memory that fits the branch lemmas **)
module type Runner = {
  proc run() : bool
}.

module Init (R : Runner) = {
  proc init() = {
                 PKEYb.skm <- empty;
                 KEYb.keys <- empty;
                 PKAEb.log <- empty;
               R_GNIKE.log <- empty;
                  R_GSAE.c <- 0;
                 R_GSAE.hs <- empty;
               R_GSAE.keys <- empty;
                R_GSAE.log <- empty;
                  SAEb.log <- empty;
                    SAEb.k <- None;
  } 

  proc run() = {
    var r;

         init();
    r <@ R.run();
    return r;
  }
}.

module Init_i (R : Runner) = {
  proc init(i) = {
    PKEYb.skm <- empty;
    KEYb.keys <- empty;
    PKAEb.log <- empty;
    R_GNIKE.log <- empty;
       R_GSAE.i <- i;
       R_GSAE.c <- 0;
      R_GSAE.hs <- empty;
    R_GSAE.keys <- empty;
     R_GSAE.log <- empty;
       SAEb.log <- empty;
         SAEb.k <- None;
  }

  proc run_i(i) = {
    var r;

         init(i);
    r <@ R.run();
    return r;
  }
}.

local lemma PreSecurity &m:
     `|  Pr[Init(A(GPKAE0(Cryptobox(E), PKEY1))).run() @ &m: res]
       - Pr[Init(A(GPKAE1(Cryptobox(E), PKEY1))).run() @ &m: res]|
  <=   `|  Pr[Init(A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY0)))).run() @ &m: res]
         - Pr[Init(A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY1)))).run() @ &m: res]|
     + `|  Pr[PreSecurity.Init(A(PreSecurity.R_GAE(PreSecurity.AE.GAE0(E, KEY1)))).run() @ &m: res]
         - Pr[PreSecurity.Init(A(PreSecurity.R_GAE(PreSecurity.AE.GAE1(E, KEY1)))).run() @ &m: res]|.
proof.
have ->:   Pr[Init(A(GPKAE0(Cryptobox(E), PKEY1))).run() @ &m: res]
         = Pr[PreSecurity.Init(A(GPKAE0(Cryptobox(E), PKEY1))).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
have ->:   Pr[Init(A(GPKAE1(Cryptobox(E), PKEY1))).run() @ &m: res]
         = Pr[PreSecurity.Init(A(GPKAE1(Cryptobox(E), PKEY1))).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
have ->:   Pr[Init(A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY0)))).run() @ &m: res]
         = Pr[PreSecurity.Init(A(PreSecurity.R_GNIKE(E, GNIKEb_mux(PKEY1, KEY0)))).run() @ &m: res].
+ byequiv=> //; proc *; inline *; sim; auto.
have ->:   Pr[Init(A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY1)))).run() @ &m: res]
         = Pr[PreSecurity.Init(A(PreSecurity.R_GNIKE(E, GNIKEb_mux(PKEY1, KEY1)))).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
by apply/(PreSecurity.PreSecurity_Init E A &m).
qed.

local module D0_Init (K : PKEY_out) = {
  proc run() = {
    var r;

    KEYb.keys <- empty;
    PKAEb.log <- empty;
            r <@ A(Count(GPKAE0(Cryptobox(E), K))).run();
    return r;
  }
}.

local module D1_Init (K : PKEY_out) = {
  proc run() = {
    var r;

    KEYb.keys <- empty;
    PKAEb.log <- empty;
            r <@ A(Count(GPKAE1(Cryptobox(E), K))).run();
    return r;
  }
}.

local lemma Switching &m:
      `|  Pr[Init(A(GPKAE0(Cryptobox(E), PKEY0))).run() @ &m: res]
        - Pr[Init(A(GPKAE1(Cryptobox(E), PKEY0))).run() @ &m: res]|
  <=   2%r * q_gen_max%r * q_csetpk_max%r * pr_guess
     + `|  Pr[Init(A(GPKAE0(Cryptobox(E), PKEY1))).run() @ &m: res]
         - Pr[Init(A(GPKAE1(Cryptobox(E), PKEY1))).run() @ &m: res]|.
proof.
have ->:   Pr[Init(A(GPKAE0(Cryptobox(E), PKEY0))).run() @ &m: res]
         = Pr[Switching.Init(D0_Init(PKEY0)).run() @ &m: res].
+ byequiv (: ={glob A, glob E} ==> ={res})=> //.
  proc; inline *; wp; call (: ={glob GPKAE0(Cryptobox(E), PKEY0)}).
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  + by proc; inline *; wp=> /=; sim.
  + by proc; inline *; sim.
  by auto.
have ->:   Pr[Init(A(GPKAE1(Cryptobox(E), PKEY0))).run() @ &m: res]
         = Pr[Switching.Init(D1_Init(PKEY0)).run() @ &m: res].
+ byequiv (: ={glob A, glob E} ==> ={res})=> //.
  proc; inline *; wp; call (: ={glob GPKAE1(Cryptobox(E), PKEY0)}).
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  + by proc; inline *; wp=> /=; sim.
  + by proc; inline *; sim.
  by auto.
have ->:   Pr[Init(A(GPKAE0(Cryptobox(E), PKEY1))).run() @ &m: res]
         = Pr[Switching.Init(D0_Init(PKEY1)).run() @ &m: res].
+ byequiv (: ={glob A, glob E} ==> ={res})=> //.
  proc; inline *; wp; call (: ={glob GPKAE0(Cryptobox(E), PKEY1)}).
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  by auto.
have ->:   Pr[Init(A(GPKAE1(Cryptobox(E), PKEY1))).run() @ &m: res]
         = Pr[Switching.Init(D1_Init(PKEY1)).run() @ &m: res].
+ byequiv (: ={glob A, glob E} ==> ={res})=> //.
  proc; inline *; wp; call (: ={glob GPKAE1(Cryptobox(E), PKEY1)}).
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  by auto.
have:= Switching.Switching_Init q_gen_max ge0_q_gen_max q_csetpk_max ge0_q_csetpk_max pr_guess _ pr_guessP
                                D0_Init _ _ &m=> //=.
print pr_guessP.
+ apply: (StdOrder.RealOrder.ler_trans (mu dkp (fun (x : _ * _)=> x.`1 = witness))).
  + exact: ge0_mu.
  exact: pr_guessP.
+ move=> K K_gen_ll K_csetpk_ll K_getsk_ll K_honpk_ll.
  proc.
  call (A_ll (Count(GPKAE0(Cryptobox(E), K))) _ _ _ _); auto.
  + by islossless; exact: E_enc_ll.
  + by islossless; exact: E_dec_ll.
  + by islossless.
  by islossless.
+ move=> K c_gen c_csetpk.
  exists * Count.q_pkenc, Count.q_pkdec; elim * => c_pkenc c_pkdec.
  proc.
  call (:    Count.q_pkenc = c_pkenc
          /\ Count.q_pkdec = c_pkdec
          /\ Switching.Counting.Count.q_gen = c_gen
          /\ Switching.Counting.Count.q_csetpk = c_csetpk
          ==>    Switching.Counting.Count.q_gen <= c_gen + q_gen_max
              /\ Switching.Counting.Count.q_csetpk <= c_csetpk + q_csetpk_max); auto.
  conseq (Count_Count0 K) (A_count c_gen c_csetpk c_pkenc c_pkdec (GPKAE0(Cryptobox(E), K)))=> /> &1.
  by exists (glob A){1} (glob E){1} (glob K){1} PKAEb.log{1} c_csetpk c_gen c_pkdec c_pkenc.
have:= Switching.Switching_Init q_gen_max ge0_q_gen_max q_csetpk_max ge0_q_csetpk_max pr_guess _ pr_guessP
                                D1_Init _ _ &m=> //=.
+ apply: (StdOrder.RealOrder.ler_trans (mu dkp (fun (x : _ * _)=> x.`1 = witness))).
  + exact: ge0_mu.
  exact: pr_guessP.
+ move=> K K_gen_ll K_csetpk_ll K_getsk_ll K_honpk_ll.
  proc.
  call (A_ll (Count(GPKAE1(Cryptobox(E), K))) _ _ _ _); auto.
  + islossless.
    + by move=> &0; apply/dctxt_ll/ge0_length.
    exact: E_enc_ll.
  + by islossless; exact: E_dec_ll.
  + by islossless.
  by islossless.
+ move=> K c_gen c_csetpk.
  exists * Count.q_pkenc, Count.q_pkdec; elim * => c_pkenc c_pkdec.
  proc.
  call (:    Count.q_pkenc = c_pkenc
          /\ Count.q_pkdec = c_pkdec
          /\ Switching.Counting.Count.q_gen = c_gen
          /\ Switching.Counting.Count.q_csetpk = c_csetpk
          ==>    Switching.Counting.Count.q_gen <= c_gen + q_gen_max
              /\ Switching.Counting.Count.q_csetpk <= c_csetpk + q_csetpk_max); auto.
  conseq (Count_Count1 K) (A_count c_gen c_csetpk c_pkenc c_pkdec (GPKAE1(Cryptobox(E), K)))=> /> &1.
  by exists (glob A){1} (glob E){1} (glob K){1} PKAEb.log{1} c_csetpk c_gen c_pkdec c_pkenc.
smt().
qed.

local module D'_Init (K : PKEY) (G : PreSecurity.AE.GAE_out) = {
  proc run() = {
    var r;

    PKEYb.skm <- empty;
            r <@ D'(K, G).run();
    return r;
  }
}.

local lemma Hybrid &m:
    `|  Pr[PreSecurity.Init(A(PreSecurity.R_GAE(PreSecurity.AE.GAE0(E, KEY1)))).run() @ &m: res]
      - Pr[PreSecurity.Init(A(PreSecurity.R_GAE(PreSecurity.AE.GAE1(E, KEY1)))).run() @ &m: res]|
  <= (bigi predT
           (fun i=> `|  Pr[Init_i(A(R_GSAE(E, GSAEb(SAE0)))).run_i(i) @ &m: res]
                      - Pr[Init_i(A(R_GSAE(E, GSAEb(SAE1)))).run_i(i) @ &m: res]|)
           0 (q_pkenc_max + q_pkdec_max + 1)).
proof.
have ->:   Pr[PreSecurity.Init(A(PreSecurity.R_GAE(PreSecurity.AE.GAE0(E, KEY1)))).run() @ &m: res]
         = Pr[Replication.Init(D'_Init(PKEY1, PreSecurity.AE.GAE0(E, KEY1))).run() @ &m: res].
+ byequiv=> //; proc; inline *; wp.
  call (: ={glob E, glob KEYb, glob PKEYb, glob PreSecurity.AE.AEb})=> //.
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  by auto=> />.
have ->:   Pr[PreSecurity.Init(A(PreSecurity.R_GAE(PreSecurity.AE.GAE1(E, KEY1)))).run() @ &m: res]
         = Pr[Replication.Init(D'_Init(PKEY1, PreSecurity.AE.GAE1(E, KEY1))).run() @ &m: res].
+ byequiv=> //; proc; inline *; wp.
  call (: ={glob E, glob KEYb, glob PKEYb, glob PreSecurity.AE.AEb})=> //.
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  by auto=> />.
have ->:   (fun i=> `|  Pr[Init_i(A(R_GSAE(E, GSAEb(SAE0)))).run_i(i) @ &m: res]
                      - Pr[Init_i(A(R_GSAE(E, GSAEb(SAE1)))).run_i(i) @ &m: res]|)
         = (fun i=> `|  Pr[Replication.Init_i(Replication.DD(E, D'_Init(PKEY1), GSAEb(SAE0))).run(i) @ &m: res]
                      - Pr[Replication.Init_i(Replication.DD(E, D'_Init(PKEY1), GSAEb(SAE1))).run(i) @ &m: res]|).
+ apply: fun_ext=> i; congr; congr; [|congr].
  + byequiv=> //; proc; inline *; wp.
    call (:    ={glob E, glob PKEYb, glob GSAEb(SAE0)}
            /\ ={log}(R_GSAE, PreSecurity.AE.AEb)
            /\ ={keys}(R_GSAE, KEYb)
            /\ ={i, c, hs}(R_GSAE, Replication.Hybrid))=> //.
    + proc; inline {2} 1=> //=.
      sp; seq  1  1: (   ={glob E, glob PKEYb, glob GSAEb(SAE0), pks, pkr, m, n, v, c, d}
                      /\ ={log}(R_GSAE, PreSecurity.AE.AEb)
                      /\ ={keys}(R_GSAE, KEYb)
                      /\ ={i, c, hs}(R_GSAE, Replication.Hybrid)
                      /\ pks0{2} = pks{2}
                      /\ pkr0{2} = pkr{2}
                      /\ m0{2} = m{2}
                      /\ n0{2} = n{2}
                      /\ d0{2} = d{2}).
      + inline *; sp; if; auto.
        sp; if; auto=> [/> /#| |/> /#].
        sp; if; auto=> [/> /#| |/> /#].
        by sp; if; auto=> /#.
      if; auto.
      inline *; sp; if; auto.
      + sp; if; auto=> [/> /#|].
        sp; if; auto.
        by call (: true); auto.
      if; auto.
      + sp; if; auto.
        by call (: true); auto.
      sp; if; auto=> [/> /#|].
      by call (: true); auto.
    + proc=> //=; inline {2} 1.
      sp; seq  1  1: (   ={glob E, glob PKEYb, glob GSAEb(SAE0), pks, pkr, c, n, v, m, d}
                      /\ ={log}(R_GSAE, PreSecurity.AE.AEb)
                      /\ ={keys}(R_GSAE, KEYb)
                      /\ ={i, c, hs}(R_GSAE, Replication.Hybrid)
                      /\ pkr0{2} = pkr{2}
                      /\ pks0{2} = pks{2}
                      /\ c0{2} = c{2}
                      /\ n0{2} = n{2}
                      /\ d0{2} = d{2}).
      + inline *; sp; if; auto.
        sp; if; auto=> [/> /#| |/> /#].
        sp; if; auto=> [/> /#| |/> /#].
        by sp; if; auto=> /#.
      if; auto.
      inline *; sp; if; auto.
      + sp; if; auto=> [/> /#|].
        sp; if; auto.
        by call (: true); auto.
      if; auto.
      + sp; if; auto.
        by call (: true); auto.
      sp; if; auto=> [/> /#|].
      by call (: true); auto.
    + by proc; inline *; sim.
    + by proc; inline *; sim.
    by auto.
  byequiv=> //; proc; inline *; wp.
  call (:    ={glob E, glob PKEYb, glob GSAEb(SAE1)}
          /\ ={log}(R_GSAE, PreSecurity.AE.AEb)
          /\ ={keys}(R_GSAE, KEYb)
          /\ ={i, c, hs}(R_GSAE, Replication.Hybrid))=> //.
  + proc; inline {2} 1=> //=.
    sp; seq  1  1: (   ={glob E, glob PKEYb, glob GSAEb(SAE1), pks, pkr, m, n, v, c, d}
                    /\ ={log}(R_GSAE, PreSecurity.AE.AEb)
                    /\ ={keys}(R_GSAE, KEYb)
                    /\ ={i, c, hs}(R_GSAE, Replication.Hybrid)
                    /\ pks0{2} = pks{2}
                    /\ pkr0{2} = pkr{2}
                    /\ m0{2} = m{2}
                    /\ n0{2} = n{2}
                    /\ d0{2} = d{2}).
    + inline *; sp; if; auto.
      sp; if; auto=> [/> /#| |/> /#].
      sp; if; auto=> [/> /#| |/> /#].
      by sp; if; auto=> /#.
    if; auto.
    inline *; sp; if; auto.
    + sp; if; auto=> [/> /#|].
      sp; if; auto.
      by call (: true); auto.
    if; auto.
    + by sp; if; auto.
    sp; if; auto=> [/> /#|].
    by call (: true); auto.
  + proc=> //=; inline {2} 1.
    sp; seq  1  1: (   ={glob E, glob PKEYb, glob GSAEb(SAE1), pks, pkr, c, n, v, m, d}
                    /\ ={log}(R_GSAE, PreSecurity.AE.AEb)
                    /\ ={keys}(R_GSAE, KEYb)
                    /\ ={i, c, hs}(R_GSAE, Replication.Hybrid)
                    /\ pkr0{2} = pkr{2}
                    /\ pks0{2} = pks{2}
                    /\ c0{2} = c{2}
                    /\ n0{2} = n{2}
                    /\ d0{2} = d{2}).
    + inline *; sp; if; auto.
      sp; if; auto=> [/> /#| |/> /#].
      sp; if; auto=> [/> /#| |/> /#].
      by sp; if; auto=> /#.
    if; auto.
    inline *; sp; if; auto.
    + sp; if; auto=> [/> /#|].
      sp; if; auto.
      by call (: true); auto.
    if; auto=> />.
    sp; if; auto=> [/> /#|].
    by call (: true); auto.
  + by proc; inline *; sim.
  + by proc; inline *; sim.
  by auto.
apply: (@Replication.Hybrid_final_Init (q_pkenc_max + q_pkdec_max) _ (D'_Init(PKEY1)) E _ _ E_enc_ll E_dec_ll &m).
+ smt(ge0_q_pkenc_max ge0_q_pkdec_max).
+ move=> G G_set_ll G_cset_ll G_enc_ll G_dec_ll.
  proc; call (A_ll (Count(GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, G), G)))); auto.
  + by islossless.
  + by islossless.
  + by islossless; exact: dkp_ll.
  by islossless.
move=> G c.
exists * Count.q_gen, Count.q_csetpk, Count.q_pkenc, Count.q_pkdec.
elim * => c_gen c_csetpk c_pkenc c_pkdec.
proc; call (:    Count.q_gen = c_gen
              /\ Count.q_csetpk = c_csetpk
              /\ Count.q_pkenc = c_pkenc
              /\ Count.q_pkdec = c_pkdec
              /\ Replication.Count.c = c
              ==> Replication.Count.c <= c + (q_pkenc_max + q_pkdec_max)); auto.
by conseq (Count_Count0' G c c_pkenc c_pkdec)
          (A_count c_gen c_csetpk c_pkenc c_pkdec (GMODPKAE_mux(PKEY1, NIKEb_mux(PKEY1, G), G)))=> /#.
qed.

lemma Security_Init &m:
     `|  Pr[Init(A(GPKAE0(Cryptobox(E), PKEY0))).run() @ &m : res]
       - Pr[Init(A(GPKAE1(Cryptobox(E), PKEY0))).run() @ &m : res]|
  <=   2%r * q_gen_max%r * q_csetpk_max%r * pr_guess
     + `|  Pr[Init(A(R_GNIKE(E, GNIKEb(GNIKE_in(PKEY1, KEY0))))).run() @ &m: res]
         - Pr[Init(A(R_GNIKE( E, GNIKEb(GNIKE_in(PKEY1, KEY1))))).run() @ &m: res]|
     + bigi predT
         (fun i=> `|  Pr[Init_i(A(R_GSAE(E, GSAE0))).run_i(i) @ &m: res]
                    - Pr[Init_i(A(R_GSAE(E, GSAE1))).run_i(i) @ &m: res]|)
         0 (q_pkenc_max + q_pkdec_max + 1).
proof.
have /StdOrder.RealOrder.ler_trans -> // := Switching &m.
rewrite -RField.addrA StdOrder.RealOrder.ler_add2l.
have /StdOrder.RealOrder.ler_trans -> // := PreSecurity &m.
rewrite StdOrder.RealOrder.ler_add2l.
exact: (Hybrid &m).
qed.

lemma Security &m:
     PKEYb.skm{m} = empty
  => KEYb.keys{m} = empty
  => PKAEb.log{m} = empty
  => R_GNIKE.log{m} = empty
  => R_GSAE.c{m} = 0
  => R_GSAE.hs{m} = empty
  => R_GSAE.keys{m} = empty
  => R_GSAE.log{m} = empty
  => SAEb.log{m} = empty
  => SAEb.k{m} = None
  =>    `|  Pr[A(GPKAE0(Cryptobox(E), PKEY0)).run() @ &m : res]
          - Pr[A(GPKAE1(Cryptobox(E), PKEY0)).run() @ &m : res]|
     <=   2%r * q_gen_max%r * q_csetpk_max%r * pr_guess
        + `|  Pr[A(R_GNIKE(E, GNIKEb(GNIKE_in(PKEY1, KEY0)))).run() @ &m: res]
            - Pr[A(R_GNIKE(E, GNIKEb(GNIKE_in(PKEY1, KEY1)))).run() @ &m: res]|
        + bigi predT
            (fun i=> `|  Pr[A_i(A, R_GSAE(E, GSAE0)).run_i(i) @ &m: res]
                       - Pr[A_i(A, R_GSAE(E, GSAE1)).run_i(i) @ &m: res]|)
            0 (q_pkenc_max + q_pkdec_max + 1).
proof.
move=> skm0 keys0 pkae_log0 rgnike_log0.
move=> rgsae_c0 rgsae_hs0 rgsae_keys0 rgsae_log0 saeb_log0 saeb_k0.
have ->:   Pr[A(GPKAE0(Cryptobox(E), PKEY0)).run() @ &m: res]
         = Pr[Init(A(GPKAE0(Cryptobox(E), PKEY0))).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
have ->:   Pr[A(GPKAE1(Cryptobox(E), PKEY0)).run() @ &m: res]
         = Pr[Init(A(GPKAE1(Cryptobox(E), PKEY0))).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
have ->:   Pr[A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY0))).run() @ &m: res]
         = Pr[Init(A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY0)))).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
have ->:   Pr[A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY1))).run() @ &m: res]
         = Pr[Init(A(R_GNIKE(E, GNIKEb_mux(PKEY1, KEY1)))).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
have ->:   (fun i=> `|  Pr[A_i(A, R_GSAE(E, GSAE0)).run_i(i) @ &m: res]
                      - Pr[A_i(A, R_GSAE(E, GSAE1)).run_i(i) @ &m: res]|)
         = (fun i=> `|  Pr[Init_i(A(R_GSAE(E, GSAE0))).run_i(i) @ &m: res]
                      - Pr[Init_i(A(R_GSAE(E, GSAE1))).run_i(i) @ &m: res]|).
+ apply: fun_ext=> i; congr; congr; [|congr].
  + by byequiv=> //; proc *; inline *; sim; auto.
  by byequiv=> //; proc *; inline *; sim; auto.
exact: (Security_Init &m).
qed.
end section Proof.

print Security.
end Security.
