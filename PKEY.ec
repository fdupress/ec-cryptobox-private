require import AllCore Distr List FSet SmtMap.

pragma +implicits.

type 'a reg = [ Honest  of 'a | Corrupt ].

op getreg (x : 'a reg) =
with x = Corrupt   => None
with x = Honest sk => Some sk.

op ogetreg = oapp getreg None<:'a>.

op is_honest (x : 'a reg option) =
  omap (fun x=> x <> Corrupt) x = Some true.

abstract theory PKEY.
require (*--*) FelTactic.

(** We use public keys as handles **)
type pkey, skey.

op dkp : { (pkey * skey) distr | is_lossless dkp } as dkp_ll.

module type PKEY_out = {
  proc gen()            : pkey option
  proc csetpk(_ : pkey) : unit option
  proc getsk(_ : pkey)  : skey option
  proc honpk(_ : pkey)  : bool option
}.

module type PKEY = {
  proc gen()            : pkey option
  proc csetpk(_ : pkey) : unit option
  proc getsk(_ : pkey)  : skey option
  proc honpk(_ : pkey)  : bool option
}.

module PKEYb = {
  var skm : (pkey, skey reg) fmap

  proc csetpk(pk : pkey) = {
    var r <- None;

    if (skm.[pk] = None) {
      skm.[pk] <- Corrupt;
             r <- Some ();
    }
    return r;
  }

  proc getsk(pk : pkey) : skey option = {
    var r <- None;
    if (   skm.[pk] <> None
        /\ skm.[pk] <> Some Corrupt) {
      r <- ogetreg skm.[pk];
    }
    return r;
  }

  proc honpk(pk : pkey) : bool option = {
    return omap (fun x=> x <> Corrupt) skm.[pk];
  }
}.

module (PKEY0 : PKEY) : PKEY_out = {
  proc gen() = {
    var sk, pk;

          (pk, sk) <$ dkp;
    PKEYb.skm.[pk] <- Honest sk;
    return Some pk;
  }

  proc csetpk = PKEYb.csetpk
  proc getsk  = PKEYb.getsk
  proc honpk  = PKEYb.honpk
}.

module (PKEY1 : PKEY) : PKEY_out = {
  proc gen() = {
    var sk, pk;
    var r <- None;

    (pk, sk) <$ dkp;
    if (PKEYb.skm.[pk] <> Some Corrupt) {
      PKEYb.skm.[pk] <- Honest sk;
                   r <- Some pk;
    }
    return r;
  }

  proc csetpk = PKEYb.csetpk
  proc getsk  = PKEYb.getsk
  proc honpk  = PKEYb.honpk
}.

module (PKEY2 : PKEY) : PKEY_out = {
  proc gen() = {
    var sk, pk;
    var r <- None;

    (pk, sk) <$ dkp;
    if (PKEYb.skm.[pk] = None) {
      PKEYb.skm.[pk] <- Honest sk;
                   r <- Some pk;
    }
    return r;
  }

  proc csetpk = PKEYb.csetpk
  proc getsk  = PKEYb.getsk
  proc honpk  = PKEYb.honpk
}.

(** Probability of distinguishing PKEY0 from PKEY1 through the interface **)
theory Switching_0_1.
module type A_PKEY (PK : PKEY_out) = {
  proc run() : bool
}.

theory Counting.
module Count (PK : PKEY_out) : PKEY_out = {
  var q_gen    : int
  var q_csetpk : int

  proc gen() = {
    var r;

        r <@ PK.gen();
    q_gen <- q_gen + 1;
    return r;
  }

  proc csetpk(pk) = {
    var r;

           r <@ PK.csetpk(pk);
    q_csetpk <- q_csetpk + 1;
    return r;
  }

  proc getsk  = PK.getsk
  proc honpk  = PK.honpk
}.

section.
declare module PK <: PKEY_out { -Count }.

equiv gen_Count: PK.gen ~ Count(PK).gen:
  ={glob PK} ==> ={glob PK, res}.
proof. by proc *; inline Count(PK).gen; sim. qed.

equiv csetpk_Count: PK.csetpk ~ Count(PK).csetpk:
  ={glob PK, arg} ==> ={glob PK, res}.
proof.
proc *; inline Count(PK).csetpk.
by wp; call (: true); auto.
qed.

equiv getsk_Count: PK.getsk ~ Count(PK).getsk:
  ={glob PK, arg} ==> ={glob PK, res}.
proof. by sim. qed.

equiv honpk_Count: PK.honpk ~ Count(PK).honpk:
  ={glob PK, arg} ==> ={glob PK, res}.
proof. by sim. qed.

equiv PK_Count (D <: A_PKEY { -PK, -Count }):
  D(PK).run ~ D(Count(PK)).run:
    ={glob D, glob PK} ==> ={glob D, glob PK, res}.
proof.
proc (={glob PK})=> //.
+ by conseq gen_Count.
+ by conseq csetpk_Count.
+ by conseq getsk_Count.
by conseq honpk_Count.
qed.
end section.
end Counting.
import Counting.

require import Mu_mem.
(*---*) import StdOrder.RealOrder.

section.
declare op q_gen_max : { int | 0 <= q_gen_max } as ge0_q_gen_max.
declare op q_csetpk_max : { int | 0 <= q_csetpk_max } as ge0_q_csetpk_max.

declare op pr_guess : { real | 0%r <= pr_guess } as ge0_pr_guess.
declare axiom dkp_max pk: mu dkp (fun (kp : _ * _)=> kp.`1 = pk) <= pr_guess.

declare module D  <: A_PKEY { -PKEYb, -Count }.

(** Adversary is lossless and makes at most q_gen_max
    (resp. q_csetpk_max) queries to its GEN (resp. CSETPK) oracle
**)
declare axiom D_ll (K <: PKEY_out { -D }):
     islossless K.gen
  => islossless K.csetpk
  => islossless K.getsk
  => islossless K.honpk
  => islossless D(K).run.

declare axiom D_count (K <: PKEY_out { -D, -Count }) c_gen c_csetpk:
  hoare [D(Count(K)).run:
                  Count.q_gen = c_gen
               /\ Count.q_csetpk = c_csetpk
           ==>    Count.q_gen <= c_gen + q_gen_max
               /\ Count.q_csetpk <= c_csetpk + q_csetpk_max].

(** The FEL requires us to bound the number of queries
    instead of just counting it.
**)
local module Bound (K : PKEY_out) : PKEY_out = {
  var q_gen    : int
  var q_csetpk : int

  proc gen() = {
    var r <- None;

    if (q_gen < q_gen_max) {
          r <@ K.gen();
      q_gen <- q_gen + 1;
    }

    return r;
  }

  proc csetpk(pk) = {
    var r <- None;

    if (q_csetpk < q_csetpk_max) {
             r <@ K.csetpk(pk);
      q_csetpk <- q_csetpk + 1;
    }
    return r;
  }

  proc getsk = K.getsk
  proc honpk = K.honpk
}.

local module PKEYbad = {
  var bad : bool
}.

local module PKEYbad0 : PKEY_out = {
  proc gen() = {
    var sk, pk;

          (pk, sk) <$ dkp;
       PKEYbad.bad <- PKEYbad.bad \/ PKEYb.skm.[pk] = Some Corrupt;
    PKEYb.skm.[pk] <- Honest sk;
    return Some pk;
  }

  proc csetpk = PKEYb.csetpk
  proc getsk  = PKEYb.getsk
  proc honpk  = PKEYb.honpk
}.

local module PKEYbad1 : PKEY_out = {
  proc gen() = {
    var sk, pk;
    var r <- None;

    (pk, sk) <$ dkp;
    if (PKEYb.skm.[pk] <> Some Corrupt) {
      PKEYb.skm.[pk] <- Honest sk;
                   r <- Some pk;
    } else {
      PKEYbad.bad <- true;
    }
    return r;
  }

  proc csetpk = PKEYb.csetpk
  proc getsk  = PKEYb.getsk
  proc honpk  = PKEYb.honpk
}.

module type Runner = {
  proc run(): bool
}.

local module InitLocals (A : Runner) : Runner = {
  proc run() = {
    var r;

         PKEYb.skm <- empty;
       Count.q_gen <- 0;
    Count.q_csetpk <- 0;
       Bound.q_gen <- 0;
    Bound.q_csetpk <- 0;
       PKEYbad.bad <- false;
                 r <@ A.run();
    return r;
  }
}.

local equiv init_locals (A <: Runner { -InitLocals }):
    A.run ~ InitLocals(A).run: ={glob A} ==> ={res}.
proof.
by proc *; inline InitLocals(A).run; wp; call (: true); auto.
qed.

local lemma Count_Bound (K <: PKEY_out { -Count, -Bound, -D }):
     islossless K.gen
  => islossless K.csetpk
  => islossless K.getsk
  => islossless K.honpk
  => equiv [
       D(Count(K)).run ~ D(Bound(K)).run:
            ={glob K, glob D}
         /\ ={q_gen, q_csetpk}(Count, Bound)
         /\ Count.q_gen{1} = 0
         /\ Count.q_csetpk{1} = 0
         ==> ={res} ].
proof.
move=> gen_ll csetpk_ll getsk_ll honpk_ll.
conseq (: ={glob K, glob D}
          /\ ={q_gen, q_csetpk}(Count, Bound)
          ==> (   Count.q_gen{1} <= q_gen_max
               => Count.q_csetpk{1} <= q_csetpk_max
               => ={res}))
       (: Count.q_gen = 0 /\ Count.q_csetpk = 0
          ==> Count.q_gen <= q_gen_max /\ Count.q_csetpk <= q_csetpk_max)=> /> //.
+ by conseq (@D_count K 0 0)=> />.
symmetry.
proc (q_gen_max < Count.q_gen \/ q_csetpk_max < Count.q_csetpk)
     (={glob K} /\ ={q_gen, q_csetpk}(Bound, Count))=> //.
+ smt().
+ exact/D_ll.
(** Gen **)
+ proc; sp; if{1}; auto.
  + by call (: true); auto.
  by call{2} gen_ll; auto=> /#.
+ by move=> _ _; proc; sp; if; auto; call gen_ll; auto.
+ by move=> _; proc; auto; call gen_ll; auto=> /#.
(** CSet **)
+ proc; sp; if{1}; auto.
  + by call (: true); auto.
  by call{2} csetpk_ll; auto=> /#.
+ by move=> _ _; proc; sp; if; auto; call csetpk_ll; auto.
+ by move=> _; proc; auto; call csetpk_ll; auto=> /#.
(** GetSK **)
+ by sim.
+ by move=> _; conseq getsk_ll.
(** HonPK **)
+ by sim.
by move=> _; conseq honpk_ll.
qed.

local phoare gen_bad:
     [PKEYbad1.gen:    !PKEYbad.bad
                    /\ card (fdom (filter (fun _=> pred1 Corrupt) PKEYb.skm)) <= q_csetpk_max
                    ==> PKEYbad.bad]
  <= (q_csetpk_max%r * pr_guess).
proof.
proc.
seq 2: (PKEYb.skm.[pk] = Some Corrupt)
       (q_csetpk_max%r * pr_guess) 1%r
       _ 0%r
       (!PKEYbad.bad)=> //.
+ by auto.
+ exists * PKEYb.skm; elim * => hm.
  conseq (: _ ==> _: ((card (fdom (filter (fun _=> pred1 Corrupt) hm)))%r * pr_guess))=> //=.
  + by move=> &0 /> _; smt(ge0_pr_guess).
  rnd (fun (x : _ * _)=> x.`1 \in (fdom (filter (fun _=> pred1 Corrupt) hm))); auto.
  move=> /> &0 _ card_le_qcsetpk; split=> [|_].
  + rewrite -/((\o) (mem (fdom (filter (fun _ => pred1 Corrupt) hm))) fst) -dmapE.
    apply: mu_mem_le.
    + by move=> pk _; rewrite dmap1E dkp_max.
  move=> [] pk sk /= _.
  by rewrite mem_fdom domE filterE=> ->.
by hoare; auto=> />.
qed.

local lemma PKEYbad &m:
     Pr[InitLocals(D(Bound(PKEYbad1))).run() @ &m: PKEYbad.bad]
  <= q_gen_max%r * q_csetpk_max%r * pr_guess.
proof.
fel 6 Bound.q_gen
      (fun _=> q_csetpk_max%r * pr_guess)
      q_gen_max
      PKEYbad.bad
      [Bound(PKEYbad1).csetpk: false;
       Bound(PKEYbad1).gen: (Bound.q_gen < q_gen_max)]
      (   Bound.q_gen <= q_gen_max
       /\ Bound.q_csetpk <= q_csetpk_max
       /\ card (fdom (filter (fun _=> pred1 Corrupt) PKEYb.skm)) <= Bound.q_csetpk)=> //=.
+ rewrite StdBigop.Bigreal.sumr_const count_predT size_range #smt:(ge0_q_gen_max).
+ auto=> &0 />.
  rewrite (: filter (fun _=> pred1 Corrupt) empty<:pkey, skey reg> = empty).
  + by apply/fmap_eqP=> pk; rewrite filterE emptyE.
  rewrite fdom0 fcards0.
  smt(ge0_q_gen_max ge0_q_csetpk_max).
+ move=> bad c_gen; proc; sp; if; inline *; auto=> />.
  move=> &0 _ _ cardB gt_b_csetpk; split=> _=> [|/#].
  split=> [/#|].
  by rewrite filter_set {1}/pred1 /= fdom_set fcardU fcard1 #smt:(fcard_ge0).
+ proc; sp; if; auto.
  by call gen_bad; auto=> /#.
+ move=> c_gen; proc; sp; if; inline *; auto.
  move=> /> &0 gt_c_b_gen _ ge_c_b_csetpk ge_card [] pk sk _; split=> /= _=> [|/#].
  by rewrite filter_set {1}/pred1 /= fdom_rem fcardD #smt:(fcard_ge0).
move=> bad c_gen; proc.
rcondf 2; first by auto=> /#.
by auto.
qed.

local equiv bad_eq:
  D(Bound(PKEYbad0)).run ~ D(Bound(PKEYbad1)).run:
    ={glob D, glob PKEYb, glob Bound, PKEYbad.bad}
    ==>    ={PKEYbad.bad}
        /\ (!PKEYbad.bad{2} => ={res}).
proof.
proc (PKEYbad.bad) (={glob Bound, glob PKEYb, PKEYbad.bad}) (={PKEYbad.bad})=> //.
+ smt().
+ exact/D_ll.
(** Gen **)
+ proc; sp; if=> //; auto.
  call (: !PKEYbad.bad{2} /\ ={glob PKEYb, PKEYbad.bad, arg}
          ==>    ={PKEYbad.bad} /\ (!PKEYbad.bad{2} => ={glob PKEYb, res}))=> //.
  + by proc; auto=> /#.
  by auto=> /#.
+ move=> &2 is_bad.
  conseq (: true ==> true) (: PKEYbad.bad = PKEYbad.bad{2} ==> PKEYbad.bad = PKEYbad.bad{2})=> //.
  + by proc; sp; if; inline *; auto=> /#.
  by islossless; rewrite dkp_ll.
+ move=> &1; proc; sp; if; auto.
  call (: PKEYbad.bad /\ PKEYbad.bad = PKEYbad.bad{1}
          ==> PKEYbad.bad /\ PKEYbad.bad = PKEYbad.bad{1}); auto.
  by proc; auto=> />; rewrite dkp_ll.
(** CSetPK **)
+ conseq (: ={res, glob Bound, glob PKEYb, PKEYbad.bad})=> //.
  by sim.
+ move=> &2 is_bad; conseq (: true ==> true)=> //.
  by islossless.
+ by move=> &1; proc; inline *; auto.
(** GetSK **)
+ conseq (: ={res, glob Bound, glob PKEYb, PKEYbad.bad})=> //.
  by sim.
+ move=> &2 is_bad; conseq (: true ==> true)=> //.
  by islossless.
+ by move=> &1; proc; auto.
(** HonPK **)
+ conseq (: ={res, glob Bound, glob PKEYb, PKEYbad.bad})=> //.
  by sim.
+ move=> &2 is_bad; conseq (: true ==> true)=> //.
  by islossless.
by move=> &1; proc; auto.
qed.

module Init (R : Runner) = {
  proc init() = {
    PKEYb.skm <- empty;
  }

  proc run() = {
    var r;

        init();
    r <@ R.run();
    return r;
  }
}.

lemma Switching_Init &m:
     `|  Pr[Init(D(PKEY0)).run() @ &m: res]
       - Pr[Init(D(PKEY1)).run() @ &m: res]|
  <= q_gen_max%r * q_csetpk_max%r * pr_guess.
proof.
have ->:   Pr[Init(D(PKEY0)).run() @ &m: res]
         = Pr[InitLocals(D(Bound(PKEYbad0))).run() @ &m: res].
+ byequiv (: ={glob D} ==> ={res})=> //.
  transitivity InitLocals(D(PKEY0)).run (={glob D} ==> ={res})
                                        (={glob D, glob PKEYb} ==> ={res})=> // [/#| |].
  + by proc; inline *; sim.
  transitivity InitLocals(D(Count(PKEY0))).run (={glob D, glob PKEYb} ==> ={res})
                                               (={glob D, glob PKEYb} ==> ={res})=> // [/#| |].
  + by proc; call (PK_Count PKEY0 D); auto.
  transitivity InitLocals(D(Bound(PKEY0))).run (={glob D, glob PKEYb} ==> ={res})
                                               (={glob D, glob PKEYb} ==> ={res})=> // [/#| |].
  + proc; call (Count_Bound PKEY0 _ _ _ _); auto=> //.
    + by islossless; exact: dkp_ll.
    + by islossless.
    + by islossless.
    by islossless.
  by sim.
have ->:   Pr[Init(D(PKEY1)).run() @ &m: res]
         = Pr[InitLocals(D(Bound(PKEYbad1))).run() @ &m: res].
+ byequiv (: ={glob D} ==> ={res})=> //.
  transitivity InitLocals(D(PKEY1)).run (={glob D} ==> ={res})
                                        (={glob D, glob PKEYb} ==> ={res})=> // [/#| |].
  + by proc; inline *; sim.
  transitivity InitLocals(D(Count(PKEY1))).run (={glob D, glob PKEYb} ==> ={res})
                                               (={glob D, glob PKEYb} ==> ={res})=> // [/#| |].
  + by proc; call (PK_Count (PKEY1) D); auto.
  transitivity InitLocals(D(Bound(PKEY1))).run (={glob D, glob PKEYb} ==> ={res})
                                               (={glob D, glob PKEYb} ==> ={res})=> // [/#| |].
  + proc; call (Count_Bound (PKEY1) _ _ _ _); auto=> //.
    + by islossless; exact: dkp_ll.
    + by islossless.
    + by islossless.
    by islossless.
  by sim.
apply/(ler_trans (Pr[InitLocals(D(Bound(PKEYbad1))).run() @ &m: PKEYbad.bad])).
+ byequiv (: ={glob D, glob PKEYb}
             ==> ={PKEYbad.bad} /\ (!PKEYbad.bad{2} => ={res}))
          : (PKEYbad.bad)=> // [|/#].
  by proc; call bad_eq; auto.
by apply/(PKEYbad &m).
qed.

lemma Switching &m:
     PKEYb.skm{m} = empty
  =>    `|Pr[D(PKEY0).run() @ &m: res] - Pr[D(PKEY1).run() @ &m: res]|
     <= q_gen_max%r * q_csetpk_max%r * pr_guess.
proof.
move=> skm0.
have ->:   Pr[D(PKEY0).run() @ &m: res]
         = Pr[Init(D(PKEY0)).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
have ->:   Pr[D(PKEY1).run() @ &m: res]
         = Pr[Init(D(PKEY1)).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
exact: Switching_Init.
qed.
end section.

end Switching_0_1.

theory Switching_1_2.
module type A_PKEY (PK : PKEY_out) = {
  proc run() : bool
}.

theory Counting.
module Count (PK : PKEY_out) : PKEY_out = {
  var q_gen    : int
  var q_csetpk : int

  proc gen() = {
    var r;

        r <@ PK.gen();
    q_gen <- q_gen + 1;
    return r;
  }

  proc csetpk(pk) = {
    var r;

           r <@ PK.csetpk(pk);
    q_csetpk <- q_csetpk + 1;
    return r;
  }

  proc getsk  = PK.getsk
  proc honpk  = PK.honpk
}.

section.
declare module PK <: PKEY_out { -Count }.

equiv gen_Count: PK.gen ~ Count(PK).gen:
  ={glob PK} ==> ={glob PK, res}.
proof. by proc *; inline Count(PK).gen; sim. qed.

equiv csetpk_Count: PK.csetpk ~ Count(PK).csetpk:
  ={glob PK, arg} ==> ={glob PK, res}.
proof.
proc *; inline Count(PK).csetpk.
by wp; call (: true); auto.
qed.

equiv getsk_Count: PK.getsk ~ Count(PK).getsk:
  ={glob PK, arg} ==> ={glob PK, res}.
proof. by sim. qed.

equiv honpk_Count: PK.honpk ~ Count(PK).honpk:
  ={glob PK, arg} ==> ={glob PK, res}.
proof. by sim. qed.

equiv PK_Count (D <: A_PKEY { -PK, -Count }):
  D(PK).run ~ D(Count(PK)).run:
    ={glob D, glob PK} ==> ={glob D, glob PK, res}.
proof.
proc (={glob PK})=> //.
+ by conseq gen_Count.
+ by conseq csetpk_Count.
+ by conseq getsk_Count.
by conseq honpk_Count.
qed.
end section.
end Counting.
import Counting.

require import Mu_mem.
(*---*) import StdOrder.RealOrder.

section.
declare op q_gen_max : { int | 0 <= q_gen_max } as ge0_q_gen_max.
declare op q_csetpk_max : { int | 0 <= q_csetpk_max } as ge0_q_csetpk_max.

declare op pr_guess : { real | 0%r <= pr_guess } as ge0_pr_guess.
declare axiom dkp_max pk: mu dkp (fun (kp : _ * _)=> kp.`1 = pk) <= pr_guess.

declare module D  <: A_PKEY { -PKEYb, -Count }.

(** Adversary is lossless and makes at most q_gen_max
    (resp. q_csetpk_max) queries to its GEN (resp. CSETPK) oracle
**)
declare axiom D_ll (K <: PKEY_out { -D }):
     islossless K.gen
  => islossless K.csetpk
  => islossless K.getsk
  => islossless K.honpk
  => islossless D(K).run.

declare axiom D_count (K <: PKEY_out { -D, -Count }) c_gen c_csetpk:
  hoare [D(Count(K)).run:
                  Count.q_gen = c_gen
               /\ Count.q_csetpk = c_csetpk
           ==>    Count.q_gen <= c_gen + q_gen_max
               /\ Count.q_csetpk <= c_csetpk + q_csetpk_max].

(** The FEL requires us to bound the number of queries
    instead of just counting it.
**)
local module Bound (K : PKEY_out) : PKEY_out = {
  var q_gen    : int
  var q_csetpk : int

  proc gen() = {
    var r <- None;

    if (q_gen < q_gen_max) {
          r <@ K.gen();
      q_gen <- q_gen + 1;
    }

    return r;
  }

  proc csetpk(pk) = {
    var r <- None;

    if (q_csetpk < q_csetpk_max) {
             r <@ K.csetpk(pk);
      q_csetpk <- q_csetpk + 1;
    }
    return r;
  }

  proc getsk = K.getsk
  proc honpk = K.honpk
}.

local module PKEYbad = {
  var bad : bool
}.

local module PKEYbad1 : PKEY_out = {
  proc gen() = {
    var sk, pk;
    var r <- None;

    (pk, sk) <$ dkp;
    if (PKEYb.skm.[pk] <> Some Corrupt) {
         PKEYbad.bad <- PKEYbad.bad \/ PKEYb.skm.[pk] <> None;
      PKEYb.skm.[pk] <- Honest sk;
                   r <- Some pk;
    }
    return r;
  }

  proc csetpk = PKEYb.csetpk
  proc getsk  = PKEYb.getsk
  proc honpk  = PKEYb.honpk
}.

local module PKEYbad2 : PKEY_out = {
  proc gen() = {
    var sk, pk;
    var r <- None;

    (pk, sk) <$ dkp;
    PKEYbad.bad <- PKEYbad.bad \/ is_honest PKEYb.skm.[pk];
    if (PKEYb.skm.[pk] = None) {
      PKEYb.skm.[pk] <- Honest sk;
                   r <- Some pk;
    } else {
    }
    return r;
  }

  proc csetpk = PKEYb.csetpk
  proc getsk  = PKEYb.getsk
  proc honpk  = PKEYb.honpk
}.

module type Runner = {
  proc run(): bool
}.

local module InitLocals (A : Runner) : Runner = {
  proc run() = {
    var r;

         PKEYb.skm <- empty;
       Count.q_gen <- 0;
    Count.q_csetpk <- 0;
       Bound.q_gen <- 0;
    Bound.q_csetpk <- 0;
       PKEYbad.bad <- false;
                 r <@ A.run();
    return r;
  }
}.

local equiv init_locals (A <: Runner { -InitLocals }):
    A.run ~ InitLocals(A).run: ={glob A} ==> ={res}.
proof.
by proc *; inline InitLocals(A).run; wp; call (: true); auto.
qed.

local lemma Count_Bound (K <: PKEY_out { -Count, -Bound, -D }):
     islossless K.gen
  => islossless K.csetpk
  => islossless K.getsk
  => islossless K.honpk
  => equiv [
       D(Count(K)).run ~ D(Bound(K)).run:
            ={glob K, glob D}
         /\ ={q_gen, q_csetpk}(Count, Bound)
         /\ Count.q_gen{1} = 0
         /\ Count.q_csetpk{1} = 0
         ==> ={res} ].
proof.
move=> gen_ll csetpk_ll getsk_ll honpk_ll.
conseq (: ={glob K, glob D}
          /\ ={q_gen, q_csetpk}(Count, Bound)
          ==> (   Count.q_gen{1} <= q_gen_max
               => Count.q_csetpk{1} <= q_csetpk_max
               => ={res}))
       (: Count.q_gen = 0 /\ Count.q_csetpk = 0
          ==> Count.q_gen <= q_gen_max /\ Count.q_csetpk <= q_csetpk_max)=> /> //.
+ by conseq (@D_count K 0 0)=> />.
symmetry.
proc (q_gen_max < Count.q_gen \/ q_csetpk_max < Count.q_csetpk)
     (={glob K} /\ ={q_gen, q_csetpk}(Bound, Count))=> //.
+ smt().
+ exact/D_ll.
(** Gen **)
+ proc; sp; if{1}; auto.
  + by call (: true); auto.
  by call{2} gen_ll; auto=> /#.
+ by move=> _ _; proc; sp; if; auto; call gen_ll; auto.
+ by move=> _; proc; auto; call gen_ll; auto=> /#.
(** CSet **)
+ proc; sp; if{1}; auto.
  + by call (: true); auto.
  by call{2} csetpk_ll; auto=> /#.
+ by move=> _ _; proc; sp; if; auto; call csetpk_ll; auto.
+ by move=> _; proc; auto; call csetpk_ll; auto=> /#.
(** GetSK **)
+ by sim.
+ by move=> _; conseq getsk_ll.
(** HonPK **)
+ by sim.
by move=> _; conseq honpk_ll.
qed.

local phoare gen_bad:
     [PKEYbad2.gen:    !PKEYbad.bad
                    /\ card (fdom (filter (fun _ x=> exists sk, x = Honest sk) PKEYb.skm)) <= q_gen_max
                    ==> PKEYbad.bad]
  <= (q_gen_max%r * pr_guess).
proof.
proc.
seq 2: (is_honest PKEYb.skm.[pk])
       (q_gen_max%r * pr_guess) 1%r
       _ 0%r
       (!PKEYbad.bad)=> //.
+ by auto.
+ exists * PKEYb.skm; elim * => skm.
  conseq (: _ ==> _: ((card (fdom (filter (fun _ x=> exists sk, x = Honest sk) skm)))%r * pr_guess))=> //=.
  + by move=> &0 /> _; smt(ge0_pr_guess).
  rnd (fun (x : _ * _)=> x.`1 \in (fdom (filter (fun _ x=> exists sk, x = Honest sk) skm))); auto.
  move=> /> &0 _ card_le_qcsetpk; split=> [|_].
  + rewrite -/((\o) (mem (fdom (filter (fun _ x=> exists sk, x = Honest sk) skm))) fst) -dmapE.
    apply: mu_mem_le.
    + by move=> pk _; rewrite dmap1E dkp_max.
  move=> [] pk sk /= _.
  by rewrite mem_fdom domE filterE /is_honest; case: skm.[pk]=> [|[]] /#.
by hoare; auto=> /#.
qed.

local lemma PKEYbad &m:
     Pr[InitLocals(D(Bound(PKEYbad2))).run() @ &m: PKEYbad.bad]
  <= q_gen_max%r * q_gen_max%r * pr_guess.
proof.
fel 6 Bound.q_gen
      (fun _=> q_gen_max%r * pr_guess)
      q_gen_max
      PKEYbad.bad
      [PKEYbad2.csetpk: false;
       Bound(PKEYbad2).gen: (Bound.q_gen < q_gen_max)]
      (   Bound.q_gen <= q_gen_max
       /\ card (fdom (filter (fun _ x=> exists sk, x = Honest sk) PKEYb.skm)) <= Bound.q_gen)=> //=.
+ rewrite StdBigop.Bigreal.sumr_const count_predT size_range #smt:(ge0_q_gen_max).
+ auto=> &0 />.
  rewrite (: filter (fun _ x=> exists sk, x = Honest sk) empty<:pkey, skey reg> = empty).
  + by apply/fmap_eqP=> pk; rewrite filterE emptyE.
  rewrite fdom0 fcards0.
  smt(ge0_q_gen_max ge0_q_csetpk_max).
+ proc; sp; if; auto.
  by call gen_bad; auto=> /#.
+ move=> c_gen; proc; sp; if; inline *; auto=> />.
  auto=> /> &0 c_lt_q _ ge_card [] pk sk _; split=> [//|/#].
  rewrite filter_set //=.
  have -> //=: exists sk', sk = sk' by exists sk.
  by rewrite fdom_set fcardU fcard1 #smt:(fcard_ge0).  
+ move=> bad c_gen; proc.
  rcondf 2; first by auto=> /#.
  by auto.
move=> bad c_gen; proc; auto=> /> &0 c_le_q card_le_c pk_notin_skm.
by rewrite filter_set {1}/pred1 /= fdom_rem fcardD #smt:(fcard_ge0).
qed.

local equiv bad_eq:
  D(Bound(PKEYbad1)).run ~ D(Bound(PKEYbad2)).run:
    ={glob D, glob PKEYb, glob Bound, PKEYbad.bad}
    ==>    ={PKEYbad.bad}
        /\ (!PKEYbad.bad{2} => ={res}).
proof.
proc (PKEYbad.bad) (={glob Bound, glob PKEYb, PKEYbad.bad}) (={PKEYbad.bad})=> //.
+ smt().
+ exact/D_ll.
(** Gen **)
+ proc; sp; if=> //; auto.
  call (: !PKEYbad.bad{2} /\ ={glob PKEYb, PKEYbad.bad, arg}
          ==>    ={PKEYbad.bad} /\ (!PKEYbad.bad{2} => ={glob PKEYb, res}))=> //.
  + by proc; auto=> /#.
  by auto=> /#.
+ move=> &2 is_bad.
  conseq (: true ==> true) (: PKEYbad.bad = PKEYbad.bad{2} ==> PKEYbad.bad = PKEYbad.bad{2})=> //.
  + by proc; sp; if; inline *; auto=> /#.
  by islossless; rewrite dkp_ll.
+ move=> &1; proc; sp; if; auto.
  call (: PKEYbad.bad /\ PKEYbad.bad = PKEYbad.bad{1}
          ==> PKEYbad.bad /\ PKEYbad.bad = PKEYbad.bad{1}); auto.
  by proc; auto=> />; rewrite dkp_ll.
(** CSetPK **)
+ conseq (: ={res, glob Bound, glob PKEYb, PKEYbad.bad})=> //.
  by sim.
+ move=> &2 is_bad; conseq (: true ==> true)=> //.
  by islossless.
+ by move=> &1; proc; inline *; auto.
(** GetSK **)
+ conseq (: ={res, glob Bound, glob PKEYb, PKEYbad.bad})=> //.
  by sim.
+ move=> &2 is_bad; conseq (: true ==> true)=> //.
  by islossless.
+ by move=> &1; proc; auto.
(** HonPK **)
+ conseq (: ={res, glob Bound, glob PKEYb, PKEYbad.bad})=> //.
  by sim.
+ move=> &2 is_bad; conseq (: true ==> true)=> //.
  by islossless.
by move=> &1; proc; auto.
qed.

module Init (R : Runner) = {
  proc init() = {
    PKEYb.skm <- empty;
  }

  proc run() = {
    var r;

        init();
    r <@ R.run();
    return r;
  }
}.

lemma Switching_Init &m:
     `|  Pr[Init(D(PKEY1)).run() @ &m: res]
       - Pr[Init(D(PKEY2)).run() @ &m: res]|
  <= q_gen_max%r * q_gen_max%r * pr_guess.
proof.
have ->:   Pr[Init(D(PKEY1)).run() @ &m: res]
         = Pr[InitLocals(D(Bound(PKEYbad1))).run() @ &m: res].
+ byequiv (: ={glob D} ==> ={res})=> //.
  transitivity InitLocals(D(PKEY1)).run (={glob D} ==> ={res})
                                        (={glob D, glob PKEYb} ==> ={res})=> // [/#| |].
  + by proc; inline *; sim.
  transitivity InitLocals(D(Count(PKEY1))).run (={glob D, glob PKEYb} ==> ={res})
                                               (={glob D, glob PKEYb} ==> ={res})=> // [/#| |].
  + by proc; call (PK_Count PKEY1 D); auto.
  transitivity InitLocals(D(Bound(PKEY1))).run (={glob D, glob PKEYb} ==> ={res})
                                               (={glob D, glob PKEYb} ==> ={res})=> // [/#| |].
  + proc; call (Count_Bound PKEY1 _ _ _ _); auto=> //.
    + by islossless; exact: dkp_ll.
    + by islossless.
    + by islossless.
    by islossless.
  by sim.
have ->:   Pr[Init(D(PKEY2)).run() @ &m: res]
         = Pr[InitLocals(D(Bound(PKEYbad2))).run() @ &m: res].
+ byequiv (: ={glob D} ==> ={res})=> //.
  transitivity InitLocals(D(PKEY2)).run (={glob D} ==> ={res})
                                        (={glob D, glob PKEYb} ==> ={res})=> // [/#| |].
  + by proc; inline *; sim.
  transitivity InitLocals(D(Count(PKEY2))).run (={glob D, glob PKEYb} ==> ={res})
                                               (={glob D, glob PKEYb} ==> ={res})=> // [/#| |].
  + by proc; call (PK_Count PKEY2 D); auto.
  transitivity InitLocals(D(Bound(PKEY2))).run (={glob D, glob PKEYb} ==> ={res})
                                               (={glob D, glob PKEYb} ==> ={res})=> // [/#| |].
  + proc; call (Count_Bound PKEY2 _ _ _ _); auto=> //.
    + by islossless; exact: dkp_ll.
    + by islossless.
    + by islossless.
    by islossless.
  by sim.
apply/(ler_trans (Pr[InitLocals(D(Bound(PKEYbad2))).run() @ &m: PKEYbad.bad])).
+ byequiv (: ={glob D, glob PKEYb}
             ==> ={PKEYbad.bad} /\ (!PKEYbad.bad{2} => ={res}))
          : (PKEYbad.bad)=> // [|/#].
  by proc; call bad_eq; auto.
by apply/(PKEYbad &m).
qed.

lemma Switching &m:
     PKEYb.skm{m} = empty
  =>    `|Pr[D(PKEY1).run() @ &m: res] - Pr[D(PKEY2).run() @ &m: res]|
     <= q_gen_max%r * q_gen_max%r * pr_guess.
proof.
move=> skm0.
have ->:   Pr[D(PKEY1).run() @ &m: res]
         = Pr[Init(D(PKEY1)).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
have ->:   Pr[D(PKEY2).run() @ &m: res]
         = Pr[Init(D(PKEY2)).run() @ &m: res].
+ by byequiv=> //; proc *; inline *; sim; auto.
exact: Switching_Init.
qed.
end section.

end Switching_1_2.
end PKEY.
